//
//  MapMenu.cpp
//  Hundreds
//
//  Created by soon on 13. 6. 5..
//
//

#include "MapMenu.h"
#include "NormalCircle.h"
#include "cocos-ext.h"
using namespace cocos2d::extension;
MapMenu::MapMenu():
m_runOn(false),
m_isEditMode(false),
m_world(NULL),
m_editMenuItem(NULL),
m_nowSelectedObject(NULL),
m_typeLabel(NULL),
m_xposLabel(NULL),
m_yposLabel(NULL),
m_xvelLabel(NULL),
m_yvelLabel(NULL),
m_parentLayer(NULL)
{

}

MapMenu::~MapMenu(){

}

bool MapMenu::init(b2World* world){
    if(!CCLayerColor::initWithColor(ccc4(200, 200, 200, 255), 480, 320)){
        return false;
    }
    //m_world = world;
    
    b2Vec2 gravity;
    gravity.Set(0.0f, 0.0f);
    m_world = new b2World(gravity);
    
    // Do we want to let bodies sleep?
    m_world->SetAllowSleeping(true);
    
    m_world->SetContinuousPhysics(true);
    
    //     m_debugDraw = new GLESDebugDraw( PTM_RATIO );
    //     world->SetDebugDraw(m_debugDraw);
    
    uint32 flags = 0;
    flags += b2Draw::e_shapeBit;
    //        flags += b2Draw::e_jointBit;
    //        flags += b2Draw::e_aabbBit;
    //        flags += b2Draw::e_pairBit;
    //        flags += b2Draw::e_centerOfMassBit;
    //m_debugDraw->SetFlags(flags);
    
    
    
    
    
    CCSprite* btn_run = CCSprite::create("pause.png");
    CCSprite* btn_run_c =  CCSprite::create("pause.png");
    btn_run_c->setColor(ccc3(150, 150, 150));
    CCSprite* btn_edit = CCSprite::create("rollbutton.png");
    CCSprite* btn_edit_c = CCSprite::create("rollbuttonSel.png");
    CCSprite* btn_remove = CCSprite::create("ButtonMinus.png");
    CCSprite* btn_remove_c = CCSprite::create("ButtonMinusSel.png");
    CCSprite* btn_save = CCSprite::create("ButtonStar.png");
    CCSprite* btn_save_c = CCSprite::create("ButtonStarSel.png");
    CCSprite* btn_newlevel = CCSprite::create("btn_levelup.png");
    CCSprite* btn_newlevel_c = CCSprite::create("btn_levelup.png");
    btn_newlevel_c->setColor(ccc3(0, 0, 135));
    CCSprite* btn_gameover = CCSprite::create("back.png");
    CCSprite* btn_gameover_c = CCSprite::create("back.png");
    btn_gameover_c->setColor(ccc3(0, 0, 135));
    
    /////////////////////////////////////////////////////////////////
    CCMenuItem* runitem = CCMenuItemSprite::create(btn_run, btn_run_c, this, menu_selector(MapMenu::onRunButtonTapped));
    runitem->setAnchorPoint(ccp(0,0));
    runitem->setPosition(ccp(0,0));
    
    m_editMenuItem = CCMenuItemSprite::create(btn_edit, btn_edit_c, this, menu_selector(MapMenu::onEditButtonTapped));
    m_editMenuItem->setAnchorPoint(ccp(0,0));
    m_editMenuItem->setPosition(ccp(70,5));
    
    CCMenuItem* removeItem = CCMenuItemSprite::create(btn_remove, btn_remove_c, this, menu_selector(MapMenu::onRemoveButtonTapped));
    removeItem->setAnchorPoint(ccp(0,0));
    removeItem->setPosition(ccp(120,5));
    
    CCMenuItem* saveItem = CCMenuItemSprite::create(btn_save, btn_save_c, this, menu_selector(MapMenu::onSaveButtonTapped));
    saveItem->setAnchorPoint(ccp(0,0));
    saveItem->setPosition(ccp(170,5));
    
    CCMenuItem* newLevelItem = CCMenuItemSprite::create(btn_newlevel, btn_newlevel_c, this, menu_selector(MapMenu::onNewLevelButtonTapped));
    newLevelItem->setAnchorPoint(ccp(0,0));
    newLevelItem->setPosition(ccp(150,60));
    newLevelItem->setScale(0.5);
    
    CCMenuItem* gameoverItem = CCMenuItemSprite::create(btn_gameover, btn_gameover_c, this, menu_selector(MapMenu::onGameOverButtonTapped));
    gameoverItem->setAnchorPoint(ccp(0,0));
    gameoverItem->setPosition(ccp(110,60));
    gameoverItem->setScale(0.5);
    
    CCMenu* controlMenu = CCMenu::create(runitem, m_editMenuItem,removeItem,saveItem,newLevelItem,gameoverItem,NULL);
    controlMenu->setPosition(ccp(240,0));
    controlMenu->setAnchorPoint(ccp(0,0));
    this->addChild(controlMenu);
    
    CCSprite* line = CCSprite::create("popup_line.png");
    line->setColor(ccc3(0, 0, 0));
    line->setPosition(ccp(235,160));
    line->setAnchorPoint(ccp(0,0));
    this->addChild(line);
    this->setTouchEnabled(true);
    
    
    //add new circle
    ///////////////////////////////////////////////////////////////////////
    NormalCircle* normal = NormalCircle::create(20, 10, 40, 280);
    normal->createBody(m_world);
    normal->getBody()->SetType(b2_staticBody);
    this->addChild(normal);
    normal->setPercentage();
    
    FixedCircle* fixed = FixedCircle::create(20, 90, 280);
    fixed->createBody(m_world);
    fixed->getBody()->SetType(b2_staticBody);
    this->addChild(fixed);
    
    MinusCircle* minus = MinusCircle::create(20, -10, 140, 280);
    minus->createBody(m_world);
    minus->getBody()->SetType(b2_staticBody);
    this->addChild(minus);
    minus->setPercentage();
    
    InfectedCircle* infected = InfectedCircle::create(20, 10, 190, 280);
    infected->createBody(m_world);
    infected->getBody()->SetType(b2_staticBody);
    this->addChild(infected);
    infected->setPercentage();
    
    OneTimeCircle* onetime = OneTimeCircle::create(20, 10, 40, 230);
    onetime->createBody(m_world);
    onetime->getBody()->SetType(b2_staticBody);
    this->addChild(onetime);
    onetime->setPercentage();
    
    SawCircle* saw = SawCircle::create(20, 90, 230);
    saw->createBody(m_world);
    saw->getBody()->SetType(b2_staticBody);
    this->addChild(saw);
    
    RedCircle* red = RedCircle::create(20, 140, 230);
    red->createBody(m_world);
    red->getBody()->SetType(b2_staticBody);
    this->addChild(red);
    
    IceCircle* ice = IceCircle::create(20, 190, 230);
    ice->createBody(m_world);
    ice->getBody()->SetType(b2_staticBody);
    this->addChild(ice);
    
    MovableBlock* block = MovableBlock::create(20, 40, 180);
    block->createBody(m_world);
    block->getBody()->SetType(b2_staticBody);
    this->addChild(block);
    
    //info
    /////////////////////////////////////////////////////////////////////////////
    m_typeLabel = CCLabelTTF::create("none", "Thonburi", 20);
    m_typeLabel->setColor(ccc3(0, 0, 0));
    m_typeLabel->setPosition(ccp(270,280));
    m_typeLabel->setAnchorPoint(ccp(0,0));
    this->addChild(m_typeLabel);
    
    m_xposLabel = CCLabelTTF::create("0", "Thonburi", 20);
    m_xposLabel->setColor(ccc3(0, 0, 0));
    m_xposLabel->setPosition(ccp(270,250));
    m_xposLabel->setAnchorPoint(ccp(0,0));
    this->addChild(m_xposLabel);
    
    m_yposLabel = CCLabelTTF::create("0", "Thonburi", 20);
    m_yposLabel->setColor(ccc3(0, 0, 0));
    m_yposLabel->setPosition(ccp(370,250));
    m_yposLabel->setAnchorPoint(ccp(0,0));
    this->addChild(m_yposLabel);
    
    m_xvelLabel = CCLabelTTF::create("0", "Thonburi", 20);
    m_xvelLabel->setColor(ccc3(0, 0, 0));
    m_xvelLabel->setPosition(ccp(270,220));
    m_xvelLabel->setAnchorPoint(ccp(0,0));
    this->addChild(m_xvelLabel);
    
    m_yvelLabel = CCLabelTTF::create("0", "Thonburi", 20);
    m_yvelLabel->setColor(ccc3(0, 0, 0));
    m_yvelLabel->setPosition(ccp(370,220));
    m_yvelLabel->setAnchorPoint(ccp(0,0));
    this->addChild(m_yvelLabel);
    
    m_stageLabel = CCLabelTTF::create("stage 0", "Thonburi", 20);
    m_stageLabel->setColor(ccc3(0, 0, 0));
    m_stageLabel->setPosition(ccp(370,280));
    m_stageLabel->setAnchorPoint(ccp(0,0));
    this->addChild(m_stageLabel);
    
    //control
    ////////////////////////////////////////////////////////////////////////////
    CCMenuItem* plusxvelitem = CCMenuItemImage::create("ButtonPlus.png", "ButtonPlusSel.png", this, menu_selector(MapMenu::onChangeVelocityButtonTapped));
    plusxvelitem->setTag(1);
    plusxvelitem->setPosition(ccp(320,230));
    plusxvelitem->setScale(0.5);
    CCMenuItem* minusxvelitem = CCMenuItemImage::create("ButtonMinus.png", "ButtonMinusSel.png", this, menu_selector(MapMenu::onChangeVelocityButtonTapped));
    minusxvelitem->setTag(2);
    minusxvelitem->setPosition(ccp(250,230));
    minusxvelitem->setScale(0.5);
    CCMenuItem* plusyvelitem = CCMenuItemImage::create("ButtonPlus.png", "ButtonPlusSel.png", this, menu_selector(MapMenu::onChangeVelocityButtonTapped));
    plusyvelitem->setTag(3);
    plusyvelitem->setPosition(ccp(420,230));
    plusyvelitem->setScale(0.5);
    CCMenuItem* minusyvelitem = CCMenuItemImage::create("ButtonMinus.png", "ButtonMinusSel.png", this, menu_selector(MapMenu::onChangeVelocityButtonTapped));
    minusyvelitem->setTag(4);
    minusyvelitem->setPosition(ccp(350,230));
    minusyvelitem->setScale(0.5);
    
    CCSprite* btn_editx = CCSprite::create("restore.png");
    CCSprite* btn_editx_c = CCSprite::create("restore.png");
    btn_editx_c->setColor(ccc3(50, 50, 50));
    CCSprite* btn_edity = CCSprite::create("restore.png");
    CCSprite* btn_edity_c = CCSprite::create("restore.png");
    btn_edity_c->setColor(ccc3(50, 50, 50));
    CCMenuItem* editxitem = CCMenuItemSprite::create(btn_editx, btn_editx_c, this, menu_selector(MapMenu::onEditPositionTapped));
    CCMenuItem* edityitem = CCMenuItemSprite::create(btn_edity, btn_edity_c, this, menu_selector(MapMenu::onEditPositionTapped));
    editxitem->setTag(1);
    editxitem->setPosition(ccp(340,260));
    editxitem->setScale(0.2);
    edityitem->setTag(2);
    edityitem->setPosition(ccp(440,260));
    edityitem->setScale(0.2);
    
    CCMenu* velocitycontrolmenu = CCMenu::create(plusxvelitem, minusxvelitem, plusyvelitem, minusyvelitem, editxitem,edityitem,NULL);
    velocitycontrolmenu->setAnchorPoint(ccp(0,0));
    velocitycontrolmenu->setPosition(ccp(0,0));
    this->addChild(velocitycontrolmenu);
    
    
    
    return true;
    
    
}

MapMenu* MapMenu::create(b2World *world){

    MapMenu *pRet = new MapMenu();
    if (pRet && pRet->init(world))
    {
        pRet->autorelease(); 
        return pRet; 
    } 
    else 
    { 
        delete pRet; 
        pRet = NULL; 
        return NULL; 
    } 

}

void MapMenu::onRunButtonTapped(){
    if(m_isEditMode){
        m_runOn = false;
        return;
    }
    m_runOn = !m_runOn;
}

void MapMenu::onEditButtonTapped(){
    if(m_runOn)
        return;
    m_isEditMode = !m_isEditMode;
    
    if(m_isEditMode == true){
        CCSprite* btn_edit = CCSprite::create("rollbutton.png");
        CCSprite* btn_edit_c = CCSprite::create("rollbuttonSel.png");
        m_editMenuItem->setNormalImage(btn_edit_c);
        m_editMenuItem->setSelectedImage(btn_edit);
    }
    else{
        CCSprite* btn_edit = CCSprite::create("rollbutton.png");
        CCSprite* btn_edit_c = CCSprite::create("rollbuttonSel.png");
        m_editMenuItem->setNormalImage(btn_edit);
        m_editMenuItem->setSelectedImage(btn_edit_c);
    }
    if(m_isEditMode == true){
        for(std::vector<HundredsObjects*>::iterator itr = m_parentLayer->getObjects().begin(); itr != m_parentLayer->getObjects().end(); itr++){
            HundredsObjects* obj = *itr;
            //obj->getBody()->SetLinearVelocity(b2Vec2(0,0));
            obj->getBody()->SetType(b2_staticBody);
        }
    }
    else{
        for(std::vector<HundredsObjects*>::iterator itr = m_parentLayer->getObjects().begin(); itr != m_parentLayer->getObjects().end(); itr++){
            HundredsObjects* obj = *itr;
            //obj->getBody()->SetLinearVelocity(b2Vec2(0,0));
            obj->getBody()->SetType(b2_dynamicBody);
        }
    }
}

void MapMenu::onRemoveButtonTapped(){
    if(!m_nowSelectedObject){
        return;
    }
    
    char* temp = new char[40];
    sprintf(temp, "%d", m_parentLayer->getStage());
    CCDictionary* stagedic = (CCDictionary*)m_parentLayer->getDic()->objectForKey(temp);
    delete[] temp;
    CCDictionary* circlesdic = (CCDictionary*)stagedic->objectForKey("circles");
    char* temp2 = new char[40];
    sprintf(temp2, "%d", m_nowSelectedObject->getIdx());
    circlesdic->removeObjectForKey(temp2);
    delete[] temp2;
    
    
    std::vector<TouchableCircle*> newSelectedObjs;
    newSelectedObjs.clear();
    for(int i = 0; i<m_parentLayer->getSelectedObjs().size(); i++){
        TouchableCircle* obj = m_parentLayer->getSelectedObjs().at(i);
        if(m_nowSelectedObject == obj){
        
        }
        else{
            newSelectedObjs.push_back(obj);
        }
    }
    m_parentLayer->setSelectedObjs(newSelectedObjs);
    
    
    std::vector<HundredsObjects*> newobjs;
    newobjs.clear();
    for(int i = 0; i < m_parentLayer->getObjects().size(); i++){
        HundredsObjects* obj = m_parentLayer->getObjects().at(i);
        if(m_nowSelectedObject == obj){
            m_parentLayer->getWorld()->DestroyBody(obj->getBody());
            m_parentLayer->removeChild(m_nowSelectedObject, true);
        }
        else{
            newobjs.push_back(obj);
        }
    }
    
    
    m_parentLayer->setObjects(newobjs);
    m_nowSelectedObject = NULL;
    m_parentLayer->setSelectedBlock(NULL);
    
}

void MapMenu::onSaveButtonTapped(){
    std::string path = CCFileUtils::sharedFileUtils()->getWriteablePath();
    path.append("stages.plist");
    
    char* temp = new char[20];
    sprintf(temp, "%d", m_parentLayer->getStage());
    CCDictionary* stagedic = (CCDictionary*)m_parentLayer->getDic()->objectForKey(temp);
    delete[] temp;
    CCDictionary* circlesdic = (CCDictionary*)stagedic->objectForKey("circles");
    
    for(int i = 0; i<m_parentLayer->getObjects().size(); i++){
        HundredsObjects* obj = m_parentLayer->getObjects().at(i);
        char* temp2 = new char[20];
        sprintf(temp2, "%d", obj->getIdx());
        CCDictionary* circledic = (CCDictionary*)circlesdic->objectForKey(temp2);
        delete[] temp2;
        if(circledic){
            char* tempx = new char[20];
            sprintf(tempx, "%0.2f", obj->getInitialXpos() / 480.0);
            CCString* posxstr = CCString::create(tempx);
            circledic->setObject(posxstr, "positionX");
            delete [] tempx;
            
            char* tempy = new char[20];
            sprintf(tempy, "%0.2f", 1.0 - obj->getInitialYpos() / 320.0);
            CCString* posystr = CCString::create(tempy);
            circledic->setObject(posystr, "positionY");
            delete [] tempy;
            
            char* tempvelx = new char[20];
            sprintf(tempvelx, "%0.1f", obj->getInitialXvel());
            CCString* velxstr = CCString::create(tempvelx);
            circledic->setObject(velxstr, "velX");
            delete [] tempvelx;
            
            char* tempvely = new char[20];
            sprintf(tempvely, "%0.1f", obj->getInitialYvel());
            CCString* velystr = CCString::create(tempvely);
            circledic->setObject(velystr, "velY");
            delete [] tempvely;
        }
        
        
        
    }
    m_parentLayer->getDic()->saveToFile(path.c_str());
}

void MapMenu::onNewLevelButtonTapped(){
    int nextlevel = m_parentLayer->getMaxStage() + 1;
    CCDictionary* leveldic = CCDictionary::create();
    char* temp = new char[20];
    sprintf(temp, "%d", nextlevel);
    m_parentLayer->getDic()->setObject(leveldic, temp);
    delete[] temp;
    
    CCString* desc = CCString::create("default");
    leveldic->setObject(desc, "description");
    
    CCDictionary* circledic = CCDictionary::create();
    leveldic->setObject(circledic, "circles");
    CCDictionary* defaultCircle = CCDictionary::create();
    circledic->setObject(defaultCircle, "1");
    CCString* typestr = CCString::create("normal");
    CCString* posxstr = CCString::create("0.5");
    CCString* posystr = CCString::create("0.2");
    CCString* velxstr = CCString::create("0");
    CCString* velystr = CCString::create("0");
    defaultCircle->setObject(typestr, "type");
    defaultCircle->setObject(posxstr, "positionX");
    defaultCircle->setObject(posystr, "positionY");
    defaultCircle->setObject(velxstr, "velX");
    defaultCircle->setObject(velystr, "velY");
    
    onSaveButtonTapped();
    m_parentLayer->setStage(nextlevel - 1);
    m_parentLayer->setMaxStage(nextlevel);
    m_parentLayer->gameOver();
    
}

void MapMenu::onChangeVelocityButtonTapped(cocos2d::CCMenuItem *item){
    if(m_nowSelectedObject == NULL)
        return;
    if(item->getTag() == 1){
        m_nowSelectedObject->setInitialXvel(m_nowSelectedObject->getInitialXvel() + 0.5);
    }
    else if(item->getTag() == 2){
        m_nowSelectedObject->setInitialXvel(m_nowSelectedObject->getInitialXvel() - 0.5);
    }
    else if(item->getTag() == 3){
        m_nowSelectedObject->setInitialYvel(m_nowSelectedObject->getInitialYvel() + 0.5);
    }
    else if(item->getTag() == 4){
        m_nowSelectedObject->setInitialYvel(m_nowSelectedObject->getInitialYvel() - 0.5);
    }
    updateInfo();
}

void MapMenu::onGameOverButtonTapped(){
    m_parentLayer->gameOver();
}

void MapMenu::onEditPositionTapped(cocos2d::CCMenuItem *item){
    if(m_nowSelectedObject == NULL)
        return;
    if(item->getTag() == 1){
        CCLayerColor* editlayer = CCLayerColor::create(ccc4(255, 255, 255, 255), 300, 200);
        editlayer->setPosition(ccp(90,60));
        CCDirector::sharedDirector()->getRunningScene()->addChild(editlayer, 10, 2);
        this->setTouchEnabled(false);
        m_parentLayer->setTouchEnabled(false);
        
        CCSprite* btn_ok = CCSprite::create("btn_ok.png");
        CCSprite* btn_ok_c = CCSprite::create("btn_ok.png");
        btn_ok_c->setColor(ccc3(0, 0, 135));
        CCMenuItem* okitem = CCMenuItemSprite::create(btn_ok, btn_ok_c, this, menu_selector(MapMenu::onEditConfirm));
        okitem->setScale(0.5);
        okitem->setPosition(ccp(-50,0));
        okitem->setTag(1);
        
        CCScale9Sprite * textfield = CCScale9Sprite::create("white-512x512.png", CCRect(0,0,200,50));
        textfield->setAnchorPoint(ccp(0,0));
        CCEditBox* editbox = CCEditBox::create(CCSize(200,50), textfield);
        editbox->setPosition(ccp(240,160));
        //editbox->setAnchorPoint(ccp(0,0));
        editbox->setFontColor(ccc3(0, 0, 0));
        char* temp = new char[20];
        sprintf(temp, "%0.2f", m_nowSelectedObject->getInitialXpos());
        editbox->setPlaceHolder(temp);
        delete[] temp;
        editlayer->addChild(editbox);
        okitem->setUserObject(editbox);
        CCSprite* btn_cancel = CCSprite::create("btn_cancel.png");
        CCSprite* btn_cancel_c = CCSprite::create("btn_cancel.png");
        btn_cancel_c->setColor(ccc3(0, 0, 135));
        CCMenuItem* cancelitem = CCMenuItemSprite::create(btn_cancel, btn_cancel_c, this, menu_selector(MapMenu::onEditCancel));
        cancelitem->setScale(0.5);
        cancelitem->setPosition(ccp(50,0));
        
        CCMenu* menu = CCMenu::create(okitem, cancelitem, NULL);
        menu->setAnchorPoint(ccp(0,0));
        menu->setPosition(ccp(150,40));
        editlayer->addChild(menu);
        
    }
    else if(item->getTag() == 2){
        CCLayerColor* editlayer = CCLayerColor::create(ccc4(255, 255, 255, 255), 300, 200);
        editlayer->setPosition(ccp(90,60));
        CCDirector::sharedDirector()->getRunningScene()->addChild(editlayer, 10, 2);
        this->setTouchEnabled(false);
        m_parentLayer->setTouchEnabled(false);
        
        CCSprite* btn_ok = CCSprite::create("btn_ok.png");
        CCSprite* btn_ok_c = CCSprite::create("btn_ok.png");
        btn_ok_c->setColor(ccc3(0, 0, 135));
        CCMenuItem* okitem = CCMenuItemSprite::create(btn_ok, btn_ok_c, this, menu_selector(MapMenu::onEditConfirm));
        okitem->setScale(0.5);
        okitem->setPosition(ccp(-50,0));
        okitem->setTag(2);
        
        CCScale9Sprite * textfield = CCScale9Sprite::create("white-512x512.png", CCRect(0,0,200,50));
        textfield->setAnchorPoint(ccp(0,0));
        CCEditBox* editbox = CCEditBox::create(CCSize(200,50), textfield);
        editbox->setPosition(ccp(240,160));
        //editbox->setAnchorPoint(ccp(0,0));
        editbox->setFontColor(ccc3(0, 0, 0));
        char* temp = new char[20];
        sprintf(temp, "%0.2f", m_nowSelectedObject->getInitialYpos());
        editbox->setPlaceHolder(temp);
        delete[] temp;
        editlayer->addChild(editbox);
        okitem->setUserObject(editbox);
        CCSprite* btn_cancel = CCSprite::create("btn_cancel.png");
        CCSprite* btn_cancel_c = CCSprite::create("btn_cancel.png");
        btn_cancel_c->setColor(ccc3(0, 0, 135));
        CCMenuItem* cancelitem = CCMenuItemSprite::create(btn_cancel, btn_cancel_c, this, menu_selector(MapMenu::onEditCancel));
        cancelitem->setScale(0.5);
        cancelitem->setPosition(ccp(50,0));
        
        CCMenu* menu = CCMenu::create(okitem, cancelitem, NULL);
        menu->setAnchorPoint(ccp(0,0));
        menu->setPosition(ccp(150,40));
        editlayer->addChild(menu);
    }
}

void MapMenu::onEditConfirm(cocos2d::CCMenuItem *item){
    if(item->getTag() == 1){
        const char* value = ((CCEditBox*)item->getUserObject())->getText();
        float ff = atof(value);
        m_nowSelectedObject->setInitialXpos(ff);
        m_nowSelectedObject->setXpos(ff);
    }
    else if(item->getTag() == 2){
        const char* value = ((CCEditBox*)item->getUserObject())->getText();
        float ff = atof(value);
        m_nowSelectedObject->setInitialYpos(ff);
        m_nowSelectedObject->setYpos(ff);
    }
    CCDirector::sharedDirector()->getRunningScene()->removeChildByTag(2, true);
    this->setTouchEnabled(true);
    m_parentLayer->setTouchEnabled(true);
}

void MapMenu::onEditCancel(){
    CCDirector::sharedDirector()->getRunningScene()->removeChildByTag(2, true);
    this->setTouchEnabled(true);
    m_parentLayer->setTouchEnabled(true);
}

void MapMenu::onEnter(){
    this->setTouchEnabled(true);
    CCLayerColor::onEnter();
}

void MapMenu::onExit(){
    CCLayerColor::onExit();
}

void MapMenu::ccTouchesBegan(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent){
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = pTouches->begin(); it != pTouches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        
        CCPoint location = touch->getLocationInView();
        
        location = CCDirector::sharedDirector()->convertToGL(location);
        float x = location.x;
        float y = location.y;
        
        //normal
        
        std::vector<HundredsObjects*> newobjs;
        newobjs.clear();
        for(int i = 0; i<m_parentLayer->getObjects().size(); i++){
            newobjs.push_back(m_parentLayer->getObjects().at(i));
        }
        if(m_isEditMode){
            std::string typestr = "";
            //normal
            if(20 < x && x < 60 && 260 < y && y < 300){
                NormalCircle* circle = NormalCircle::create(DEFAULT_RADIUS,0,240,160);
                circle->createBody(m_parentLayer->getWorld());
                m_parentLayer->addChild(circle);
                circle->setPercentage();
                newobjs.push_back(circle);
                //m_nowSelectedObject = circle;
                typestr = "normal";
            }
            //fixed
            if(70 < x && x < 110 && 260 < y && y < 300){
                FixedCircle* circle = FixedCircle::create(30, 240, 160);
                circle->createBody(m_parentLayer->getWorld());
                m_parentLayer->addChild(circle);
                newobjs.push_back(circle);
                //m_nowSelectedObject = circle;
                typestr = "fixed";
            }
            //minus
            if(120 < x && x < 160 && 260 < y && y < 300){
                MinusCircle* circle = MinusCircle::create(20, -10,240, 160);
                circle->createBody(m_parentLayer->getWorld());
                circle->setPercentage();
                m_parentLayer->addChild(circle);
                newobjs.push_back(circle);
                //m_nowSelectedObject = circle;
                typestr = "minus";
            }
            //infected
            if(170 < x && x < 210 && 260 < y && y < 300){
                InfectedCircle* circle = InfectedCircle::create(DEFAULT_RADIUS + 10, 10, 240, 160);
                circle->createBody(m_parentLayer->getWorld());
                m_parentLayer->addChild(circle);
                circle->setPercentage();
                newobjs.push_back(circle);
                //m_nowSelectedObject = circle;
                typestr = "infected";
            }
            //onetime
            if(20 < x && x < 60 && 210 < y && y < 250){
                OneTimeCircle* circle = OneTimeCircle::create(DEFAULT_RADIUS,0,240,160);
                circle->createBody(m_parentLayer->getWorld());
                m_parentLayer->addChild(circle);
                circle->setPercentage();
                newobjs.push_back(circle);
                m_nowSelectedObject = circle;
                typestr = "onetime";
            }
            //saw
            if(70 < x && x < 110 && 210 < y && y < 250){
                SawCircle* circle = SawCircle::create(DEFAULT_RADIUS, 240, 160);
                circle->createBody(m_parentLayer->getWorld());
                circle->getBody()->ApplyAngularImpulse(20);
                m_parentLayer->addChild(circle);
                newobjs.push_back(circle);
                typestr = "saw";
            }
            //red
            if(120 < x && x < 160 && 210 < y && y < 250){
                RedCircle* circle = RedCircle::create(DEFAULT_RADIUS, 240, 160);
                circle->createBody(m_parentLayer->getWorld());
                m_parentLayer->addChild(circle);
                newobjs.push_back(circle);
                typestr = "red";
            }
            //ice
            if(170 < x && x < 210 && 210 < y && y < 250){
                IceCircle* circle = IceCircle::create(DEFAULT_RADIUS, 240, 160);
                circle->createBody(m_parentLayer->getWorld());
                m_parentLayer->addChild(circle);
                newobjs.push_back(circle);
                typestr = "ice";
            }
            //movable
            if(20 < x && x < 60 && 160 < y && y < 200){
                MovableBlock* circle = MovableBlock::create(DEFAULT_RADIUS, 240, 160);
                circle->createBody(m_parentLayer->getWorld());
                m_parentLayer->addChild(circle);
                newobjs.push_back(circle);
                typestr = "movable";
            }
            if(typestr != ""){
                newobjs.back()->setInitialXpos(240);
                newobjs.back()->setInitialYpos(160);
                newobjs.back()->setInitialXvel(0);
                newobjs.back()->setInitialYvel(0);
                m_parentLayer->setObjects(newobjs);
                
                char* temp = new char[20];
                sprintf(temp, "%d", m_parentLayer->getStage());
                CCDictionary* leveldic = (CCDictionary*)m_parentLayer->getDic()->objectForKey(temp);
                delete [] temp;
                
                CCDictionary* circledic = (CCDictionary*)leveldic->objectForKey("circles");
                int circlenum = 0;
                if(circledic->allKeys() != NULL){
                    circlenum = circledic->allKeys()->count();
                }
                
                CCDictionary* newcircle = CCDictionary::create();
                char* temp2 = new char[20];
                sprintf(temp2, "%d", circlenum + 1);
                circledic->setObject(newcircle, temp2);
                delete[] temp2;
                
                newobjs.back()->setIdx(circlenum+1);
                
                CCString* typecstr = CCString::create(typestr);
                CCString* posxstr = CCString::create("0.5");
                CCString* posystr = CCString::create("0.5");
                CCString* velxstr = CCString::create("0");
                CCString* velystr = CCString::create("0");
                
                newcircle->setObject(typecstr, "type");
                newcircle->setObject(posxstr, "positionX");
                newcircle->setObject(posystr, "positionY");
                newcircle->setObject(velxstr, "velX");
                newcircle->setObject(velystr, "velY");
                
                m_nowSelectedObject = NULL;
            }
        }
        updateInfo();
        
        
        
        
    }
}

void MapMenu::ccTouchesEnded(cocos2d::CCSet *touches, cocos2d::CCEvent *event){
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        
        CCPoint location = touch->getLocationInView();
        
        location = CCDirector::sharedDirector()->convertToGL(location);
        
    }
}

void MapMenu::ccTouchesMoved(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent){
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = pTouches->begin(); it != pTouches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        
        CCPoint location = touch->getLocationInView();
        
        location = CCDirector::sharedDirector()->convertToGL(location);
        
    }
}

void MapMenu::updateInfo(){
    
    //stage
    char* temp5 = new char[20];
    sprintf(temp5, "stage %d", m_parentLayer->getStage());
    m_stageLabel->setString(temp5);
    delete[] temp5;
    
    if(m_nowSelectedObject == NULL)
        return;
    
    NormalCircle* normal = dynamic_cast<NormalCircle*>(m_nowSelectedObject);
    FixedCircle* fixed = dynamic_cast<FixedCircle*>(m_nowSelectedObject);
    MinusCircle* minus = dynamic_cast<MinusCircle*>(m_nowSelectedObject);
    InfectedCircle* infected = dynamic_cast<InfectedCircle*>(m_nowSelectedObject);
    OneTimeCircle* onetime = dynamic_cast<OneTimeCircle*>(m_nowSelectedObject);
    SawCircle* saw = dynamic_cast<SawCircle*>(m_nowSelectedObject);
    RedCircle* red = dynamic_cast<RedCircle*>(m_nowSelectedObject);
    IceCircle* ice = dynamic_cast<IceCircle*>(m_nowSelectedObject);
    MovableBlock* movable = dynamic_cast<MovableBlock*>(m_nowSelectedObject);
    if(normal)
        m_typeLabel->setString("normal");
    else if(fixed)
        m_typeLabel->setString("fixed");
    else if(minus)
        m_typeLabel->setString("minus");
    else if(infected)
        m_typeLabel->setString("infected");
    else if(onetime)
        m_typeLabel->setString("onetime");
    else if(saw)
        m_typeLabel->setString("saw");
    else if(red)
        m_typeLabel->setString("red");
    else if(ice)
        m_typeLabel->setString("ice");
    else if(movable)
        m_typeLabel->setString("movable");
    
    //xpos
    char* temp1 = new char[20];
    sprintf(temp1, "%0.1f", m_nowSelectedObject->getInitialXpos());
    m_xposLabel->setString(temp1);
    delete[] temp1;
    //ypos
    char* temp2 = new char[20];
    sprintf(temp2, "%0.1f", m_nowSelectedObject->getInitialYpos());
    m_yposLabel->setString(temp2);
    delete[] temp2;
    //xvel
    char* temp3 = new char[20];
    sprintf(temp3, "%0.1f", m_nowSelectedObject->getInitialXvel());
    m_xvelLabel->setString(temp3);
    delete[] temp3;
    //yvel
    char* temp4 = new char[20];
    sprintf(temp4, "%0.1f", m_nowSelectedObject->getInitialYvel());
    m_yvelLabel->setString(temp4);
    delete[] temp4;
    
}

