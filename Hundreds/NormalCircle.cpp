//
//  NormalCircle.cpp
//  Hundreds
//
//  Created by soon on 13. 6. 2..
//
//

#include "NormalCircle.h"

static CCGLProgram* s_pShader = NULL;
static int s_nColorLocation = -1;
static ccColor4F s_tColor = {1.0f,1.0f,1.0f,1.0f};
static int s_nPointSizeLocation = -1;

HundredsObjects::HundredsObjects():
m_body(NULL),
m_type(0),
m_xpos(0),
m_ypos(0),
m_radius(10),
m_initialXpos(0),
m_initialYpos(0),
m_initialXvel(0),
m_initialYvel(0),
m_idx(0)
{

}

HundredsObjects::~HundredsObjects(){

}

void HundredsObjects::setGLColor(float r, float g, float b, float a){
    s_tColor.r = r;
    s_tColor.g = g;
    s_tColor.b = b;
    s_tColor.a = a;
}
void HundredsObjects::drawFilledCircle(cocos2d::CCPoint center, float r, int totalSegs){
    
    if(s_pShader == NULL){
        s_pShader = CCShaderCache::sharedShaderCache()->programForKey(kCCShader_Position_uColor);
        
        s_nColorLocation = glGetUniformLocation( s_pShader->getProgram(), "u_color");
        CHECK_GL_ERROR_DEBUG();
        s_nPointSizeLocation = glGetUniformLocation( s_pShader->getProgram(), "u_pointSize");
        CHECK_GL_ERROR_DEBUG();
    }
    
    int additionalSegment = 1;
    
    const float coef = 2.0f * (float)M_PI/totalSegs;
    
    GLfloat *vertices = (GLfloat*)calloc( sizeof(GLfloat)*2*(totalSegs+2), 1);
    if( ! vertices )
        return;
    
    for(unsigned int i = 0;i <= totalSegs; i++) {
        float rads = i*coef;
        GLfloat j = r * cosf(rads) + center.x;
        GLfloat k = r * sinf(rads) + center.y;
        
        vertices[i*2] = j;
        vertices[i*2+1] = k;
    }
    vertices[(totalSegs+1)*2] = center.x;
    vertices[(totalSegs+1)*2+1] = center.y;
    
    s_pShader->use();
    s_pShader->setUniformForModelViewProjectionMatrix();
    s_pShader->setUniformLocationWith4fv(s_nColorLocation, (GLfloat*) &s_tColor.r, 1);
    
    ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
    
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glDrawArrays(GL_TRIANGLE_FAN, 0, (GLsizei) totalSegs+additionalSegment);
    
    free( vertices );
    
    CC_INCREMENT_GL_DRAWS(1);

}

////////////////////////////////////////////////////////
TouchableCircle::TouchableCircle():
m_percentLabel(NULL),
m_point(0),
m_freezePoint(0),
m_forceBeforeFreeze(),
m_phoenixCount(0)
#ifdef TOGGLE_CONTROL_MODE
,m_toggleOn(false)
#endif
{

}

TouchableCircle::~TouchableCircle(){

}

void TouchableCircle::freeze(){
    m_freezePoint = DEFAULT_FREEZE_POINT;
    m_body->SetType(b2_staticBody);
}

void TouchableCircle::warmed(){
    m_freezePoint = 0;
    m_body->SetType(b2_dynamicBody);
    m_body->SetLinearVelocity(m_forceBeforeFreeze);
    m_forceBeforeFreeze = b2Vec2(0,0);
    m_phoenixCount = DEFAULT_PHOENIX_COUNT;
}


////////////////////////////////////////////////////////



NormalCircle::NormalCircle()
{

}

NormalCircle::~NormalCircle(){

}

bool NormalCircle::init(float radius, int point, float xpos, float ypos){
    if(!CCSprite::init()){
        return false;
    }
    m_radius = radius;
    m_xpos = xpos;
    m_ypos = ypos;
    m_point = point;
    return true;
}

NormalCircle* NormalCircle::create(const float radius, const int point, const float xpos, const float ypos){
    NormalCircle *pobSprite = new NormalCircle();
    if (pobSprite && pobSprite->init(radius, point, xpos, ypos))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}

void NormalCircle::createBody(b2World *world){
    b2BodyDef ballBodyDef;
    ballBodyDef.type = b2_dynamicBody;
    ballBodyDef.position.Set(m_xpos/PTM_RATIO, m_ypos/PTM_RATIO);
    ballBodyDef.userData = this;
    b2Body* body = world->CreateBody(&ballBodyDef);
    m_body = body;
    
    b2CircleShape circle;
    circle.m_radius = m_radius/PTM_RATIO;
    b2FixtureDef ballShapeDef;
    ballShapeDef.shape = &circle;
    ballShapeDef.density = 1.0f;
    ballShapeDef.friction = 0.0f;
    ballShapeDef.restitution = 1.0f;
    body->CreateFixture(&ballShapeDef);
    
    m_type = 0;
}

void NormalCircle::setPercentage(){
    
    char* temp = new char[20];
    sprintf(temp, "%0.0f",m_point);
    
    m_percentLabel = CCLabelTTF::create(temp, "Thonburi", 20);
    delete[] temp;
    m_percentLabel->setColor(ccc3(0, 0, 0));
    this->addChild(m_percentLabel);
    //m_percentLabel->retain();
    m_percentLabel->setPosition(ccp(m_xpos,m_ypos));
}

void NormalCircle::draw(){

    m_percentLabel->setPosition(ccp(m_xpos, m_ypos));
    //m_percentLabel->draw();

    float radi = this->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO;
    
    if(m_freezePoint != 0){
        setGLColor(0, 0, 1, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), radi, 50);
        setGLColor(1, 1, 1, 1);
        if(m_toggleOn)
            setGLColor(1, 0, 0, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), radi - ((float)m_freezePoint / DEFAULT_FREEZE_POINT * 10), 50);
    }
    else{
        setGLColor(1, 1, 1, 1);
        if(m_toggleOn)
            setGLColor(1, 0, 0, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), radi, 50);
    }
    //ccDrawCircle(ccp(this->getPosition().x,this->getPosition().y), radi, 0, 100, false, 1, 1);

}


////////////////////////////////////////////////////////////////

FixedCircle::FixedCircle()
{
    
}

FixedCircle::~FixedCircle(){
    
}

bool FixedCircle::init(float radius, float xpos, float ypos){
    if(!CCSprite::init()){
        return false;
    }
    m_radius = radius;
    m_xpos = xpos;
    m_ypos = ypos;
    return true;
}

FixedCircle* FixedCircle::create(const float radius, const float xpos, const float ypos){
    FixedCircle *pobSprite = new FixedCircle();
    if (pobSprite && pobSprite->init(radius, xpos, ypos))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}

void FixedCircle::createBody(b2World *world){
    
    b2BodyDef ballBodyDef;
    ballBodyDef.type = b2_staticBody;
    ballBodyDef.position.Set(m_xpos/PTM_RATIO, m_ypos/PTM_RATIO);
    ballBodyDef.userData = this;
    b2Body* body = world->CreateBody(&ballBodyDef);
    m_body = body;
    
    b2CircleShape circle;
    circle.m_radius = m_radius/PTM_RATIO;
    b2FixtureDef ballShapeDef;
    ballShapeDef.shape = &circle;
    ballShapeDef.density = 1.0f;
    ballShapeDef.friction = 0.0f;
    ballShapeDef.restitution = 1.0f;
    body->CreateFixture(&ballShapeDef);
    
    m_type = 1;
    
}

void FixedCircle::draw(){
    float radi = this->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO;
    setGLColor(0.5, 0.5, 0.5, 1);
    drawFilledCircle(ccp(m_xpos, m_ypos), radi, 50);
}

////////////////////////////////////////////////////////////////////

MinusCircle::MinusCircle(){

}

MinusCircle::~MinusCircle(){

}

bool MinusCircle::init(float radius, int point, float xpos, float ypos){
    if(!CCSprite::init()){
        return false;
    }
    m_radius = radius;
    m_xpos = xpos;
    m_ypos = ypos;
    m_point = point;
    return true;
}

MinusCircle* MinusCircle::create(const float radius, const int point, const float xpos, const float ypos){
    MinusCircle *pobSprite = new MinusCircle();
    if (pobSprite && pobSprite->init(radius, point, xpos, ypos))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}

void MinusCircle::createBody(b2World *world){
    b2BodyDef ballBodyDef;
    ballBodyDef.type = b2_dynamicBody;
    ballBodyDef.position.Set(m_xpos/PTM_RATIO, m_ypos/PTM_RATIO);
    ballBodyDef.userData = this;
    b2Body* body = world->CreateBody(&ballBodyDef);
    m_body = body;
    
    b2CircleShape circle;
    circle.m_radius = m_radius/PTM_RATIO;
    b2FixtureDef ballShapeDef;
    ballShapeDef.shape = &circle;
    ballShapeDef.density = 1.0f;
    ballShapeDef.friction = 0.0f;
    ballShapeDef.restitution = 1.0f;
    body->CreateFixture(&ballShapeDef);
    
    m_type = 2;
}

void MinusCircle::setPercentage(){
    
    char* temp = new char[20];
    sprintf(temp, "%0.0f",m_point);
    
    m_percentLabel = CCLabelTTF::create(temp, "Thonburi", -m_point + 20);
    delete[] temp;
    m_percentLabel->setColor(ccc3(0, 0, 0));
    this->addChild(m_percentLabel);
    m_percentLabel->setPosition(ccp(m_xpos,m_ypos));
}

void MinusCircle::draw(){
    
    m_percentLabel->setPosition(ccp(m_xpos, m_ypos));
    //m_percentLabel->draw();
    
    float radi = this->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO;
    
    if(m_freezePoint != 0){
        setGLColor(0, 0, 1, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), radi, 50);
        
        setGLColor(1, 1, 1, 1);
        if(m_toggleOn)
            setGLColor(1, 0, 0, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), radi - ((float)m_freezePoint / DEFAULT_FREEZE_POINT * 10), 50);
    }
    else{
        setGLColor(1, 1, 1, 1);
        if(m_toggleOn)
            setGLColor(1, 0, 0, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), radi, 50);
    }
    
    //ccDrawCircle(ccp(this->getPosition().x,this->getPosition().y), radi, 0, 100, false, 1, 1);
    
}
////////////////////////////////////////////////////////////////////////////////////////

InfectedCircle::InfectedCircle(){

}

InfectedCircle::~InfectedCircle(){

}

bool InfectedCircle::init(float radius, int point, float xpos, float ypos){
    if(!CCSprite::init()){
        return false;
    }
    m_radius = radius;
    m_xpos = xpos;
    m_ypos = ypos;
    m_point = point;
    return true;
}

InfectedCircle* InfectedCircle::create(const float radius, const int point, const float xpos, const float ypos){
    InfectedCircle *pobSprite = new InfectedCircle();
    if (pobSprite && pobSprite->init(radius, point, xpos, ypos))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}

void InfectedCircle::createBody(b2World *world){
    b2BodyDef ballBodyDef;
    ballBodyDef.type = b2_dynamicBody;
    ballBodyDef.position.Set(m_xpos/PTM_RATIO, m_ypos/PTM_RATIO);
    ballBodyDef.userData = this;
    b2Body* body = world->CreateBody(&ballBodyDef);
    m_body = body;
    
    b2CircleShape circle;
    circle.m_radius = m_radius/PTM_RATIO;
    b2FixtureDef ballShapeDef;
    ballShapeDef.shape = &circle;
    ballShapeDef.density = 1.0f;
    ballShapeDef.friction = 0.0f;
    ballShapeDef.restitution = 1.0f;
    body->CreateFixture(&ballShapeDef);
    m_remainTimeToReduce = INFECTED_REDUCING_TIME;
    m_type = 4;
}

void InfectedCircle::setPercentage(){
    
    char* temp = new char[20];
    sprintf(temp, "%0.0f",m_point);
    
    m_percentLabel = CCLabelTTF::create(temp, "Thonburi", 20 + m_point);
    delete[] temp;
    m_percentLabel->setColor(ccc3(0, 0, 0));
    this->addChild(m_percentLabel);
    m_percentLabel->setPosition(ccp(m_xpos,m_ypos));
}

void InfectedCircle::draw(){
    
    m_percentLabel->setPosition(ccp(m_xpos, m_ypos));
    //m_percentLabel->draw();
    
    float radi = this->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO;
    float innerradi = radi - 10;
    
    
    if(m_freezePoint != 0){
        setGLColor(0, 0, 1, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), radi, 50);
        setGLColor(0.5, 0.5, 0.5, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), radi - ((float)m_freezePoint / DEFAULT_FREEZE_POINT * 10), 50);
        setGLColor(1, 1, 1, 1);
        if(m_toggleOn)
            setGLColor(1, 0, 0, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), innerradi, 50);
    }
    else{
        setGLColor(0.5, 0.5, 0.5, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), radi, 50);
        setGLColor(1, 1, 1, 1);
        if(m_toggleOn)
            setGLColor(1, 0, 0, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), innerradi, 40);
    }
    
    //ccDrawCircle(ccp(this->getPosition().x,this->getPosition().y), radi, 0, 100, false, 1, 1);
    
}

void InfectedCircle::reduce(){
    if(m_freezePoint != 0){
        m_remainTimeToReduce = INFECTED_REDUCING_TIME;
        return;
    }
    if(m_point == 0){
        m_remainTimeToReduce = INFECTED_REDUCING_TIME;
        return;
    }
        
    m_remainTimeToReduce--;
    if(m_remainTimeToReduce < 0){
        m_point--;
        m_radius--;
        m_body->GetFixtureList()->GetShape()->m_radius = m_radius / PTM_RATIO;
        
        char* temp = new char[20];
        sprintf(temp, "%0.0f",m_point);
        m_percentLabel->setString(temp);
        m_percentLabel->setFontSize(m_percentLabel->getFontSize() - 1);
        delete[] temp;
        
        m_remainTimeToReduce = INFECTED_REDUCING_TIME;
    }
}

////////////////////////////////////////////////////////////////////////////////////////
OneTimeCircle::OneTimeCircle()
{
    
}

OneTimeCircle::~OneTimeCircle(){
    
}

bool OneTimeCircle::init(float radius, int point, float xpos, float ypos){
    if(!CCSprite::init()){
        return false;
    }
    m_radius = radius;
    m_xpos = xpos;
    m_ypos = ypos;
    m_point = point;
    m_touched = false;
    return true;
}

OneTimeCircle* OneTimeCircle::create(const float radius, const int point, const float xpos, const float ypos){
    OneTimeCircle *pobSprite = new OneTimeCircle();
    if (pobSprite && pobSprite->init(radius, point, xpos, ypos))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}

void OneTimeCircle::createBody(b2World *world){
    b2BodyDef ballBodyDef;
    ballBodyDef.type = b2_dynamicBody;
    ballBodyDef.position.Set(m_xpos/PTM_RATIO, m_ypos/PTM_RATIO);
    ballBodyDef.userData = this;
    b2Body* body = world->CreateBody(&ballBodyDef);
    m_body = body;
    
    b2CircleShape circle;
    circle.m_radius = m_radius/PTM_RATIO;
    b2FixtureDef ballShapeDef;
    ballShapeDef.shape = &circle;
    ballShapeDef.density = 1.0f;
    ballShapeDef.friction = 0.0f;
    ballShapeDef.restitution = 1.0f;
    body->CreateFixture(&ballShapeDef);
    
    m_type = 5;
}

void OneTimeCircle::setPercentage(){
    
    char* temp = new char[20];
    sprintf(temp, "%0.0f",m_point);
    
    m_percentLabel = CCLabelTTF::create(temp, "Thonburi", 20);
    delete[] temp;
    m_percentLabel->setColor(ccc3(0, 0, 0));
    this->addChild(m_percentLabel);
    //m_percentLabel->retain();
    m_percentLabel->setPosition(ccp(m_xpos,m_ypos));
}

void OneTimeCircle::draw(){
    
    m_percentLabel->setPosition(ccp(m_xpos, m_ypos));
    //m_percentLabel->draw();
    
    float radi = this->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO;
    
    if(m_freezePoint != 0){
        setGLColor(0, 0, 1, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), radi, 50);
        setGLColor(0, 1, 0, 1);
        if(m_toggleOn)
            setGLColor(1, 0, 0, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), radi - ((float)m_freezePoint / DEFAULT_FREEZE_POINT * 10), 50);
    }
    else{
        setGLColor(0, 1, 0, 1);
        if(m_toggleOn)
            setGLColor(1, 0, 0, 1);
        drawFilledCircle(ccp(m_xpos, m_ypos), radi, 50);
    }
    //ccDrawCircle(ccp(this->getPosition().x,this->getPosition().y), radi, 0, 100, false, 1, 1);
    
}



//////////////////////////////////////////////////////////////////////////////////////////

SawCircle::SawCircle():
m_sharp(NULL)
{

}

SawCircle::~SawCircle(){

    delete[] m_sharp;
}

bool SawCircle::init(float radius, float xpos, float ypos){
    if(!CCSprite::init()){
        return false;
    }
    m_radius = radius;
    m_xpos = xpos;
    m_ypos = ypos;
    m_sharp = new CCPoint[8];
    return true;
}

SawCircle* SawCircle::create(const float radius, const float xpos, const float ypos){
    SawCircle *pobSprite = new SawCircle();
    if (pobSprite && pobSprite->init(radius, xpos, ypos))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}

void SawCircle::createBody(b2World *world){
    b2BodyDef ballBodyDef;
    ballBodyDef.type = b2_dynamicBody;
    ballBodyDef.position.Set(m_xpos/PTM_RATIO, m_ypos/PTM_RATIO);
    ballBodyDef.userData = this;
    b2Body* body = world->CreateBody(&ballBodyDef);
    m_body = body;
    
    b2CircleShape circle;
    circle.m_radius = m_radius/PTM_RATIO;
    b2FixtureDef ballShapeDef;
    ballShapeDef.shape = &circle;
    ballShapeDef.density = 1.0f;
    ballShapeDef.friction = 0.0f;
    ballShapeDef.restitution = 1.0f;
    body->CreateFixture(&ballShapeDef);
    
    m_type = 3;
}

void SawCircle::draw(){
    
    float radi = this->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO;
    setGLColor(0, 1, 0, 1);
    drawFilledCircle(ccp(m_xpos, m_ypos), radi - 10, 50);
    float angle = m_body->GetAngle() * 10;
    float antorad = (float)M_PI/ 180.0;

    
    m_sharp[0] = CCPoint(m_xpos + sin(angle * antorad) * (radi+5), m_ypos + cos(angle * antorad) * (radi+5));
    m_sharp[1] = CCPoint(m_xpos + sin((angle +45) * antorad) * (radi - 10), m_ypos + cos((angle + 45) * antorad) * (radi-10));
    m_sharp[2] = CCPoint(m_xpos + sin((angle+90) * antorad) * (radi+5), m_ypos + cos((angle+90) * antorad) * (radi+5));
    m_sharp[3] = CCPoint(m_xpos + sin((angle +45+90) * antorad) * (radi - 10), m_ypos + cos((angle + 45+90) * antorad) * (radi - 10));
    m_sharp[4] = CCPoint(m_xpos + sin((angle+180) * antorad) * (radi+5), m_ypos + cos((angle+180)*antorad) * (radi+5));
    m_sharp[5] = CCPoint(m_xpos + sin((angle +45+180) * antorad) * (radi - 10), m_ypos + cos((angle + 45+180) * antorad) * (radi - 10));
    m_sharp[6] = CCPoint(m_xpos + sin((angle+270)*antorad) * (radi+5), m_ypos + cos((angle+270)*antorad) * (radi+5));
    m_sharp[7] = CCPoint(m_xpos + sin((angle +45+270) * antorad) * (radi - 10), m_ypos + cos((angle + 45+270) * antorad) * (radi - 10));
    
    //CCLog("%f %f : %f %f : %f %f : %f %f : %f %f : %f %f : %f %f : %f %f", m_sharp[0].x, m_sharp[0].y, m_sharp[1].x, m_sharp[1].y, m_sharp[2].x, m_sharp[2].y, m_sharp[3].x, m_sharp[3].y, m_sharp[4].x, m_sharp[4].y, m_sharp[5].x, m_sharp[5].y, m_sharp[6].x, m_sharp[6].y, m_sharp[7].x, m_sharp[7].y);
    
    //ccDrawSolidPoly(m_sharp, 8, ccc4f(1, 1, 0, 1));
    ccDrawPoly(m_sharp, 8, true);

    //ccDrawCircle(ccp(this->getPosition().x,this->getPosition().y), radi, 0, 100, false, 1, 1);
    
}

////////////////////////////////////////////////////////////////////////////////


RedCircle::RedCircle(){
    
}

RedCircle::~RedCircle(){
    
    
}

bool RedCircle::init(float radius, float xpos, float ypos){
    if(!CCSprite::init()){
        return false;
    }
    m_radius = radius;
    m_xpos = xpos;
    m_ypos = ypos;
    return true;
}

RedCircle* RedCircle::create(const float radius, const float xpos, const float ypos){
    RedCircle *pobSprite = new RedCircle();
    if (pobSprite && pobSprite->init(radius, xpos, ypos))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}

void RedCircle::createBody(b2World *world){
    b2BodyDef ballBodyDef;
    ballBodyDef.type = b2_dynamicBody;
    ballBodyDef.position.Set(m_xpos/PTM_RATIO, m_ypos/PTM_RATIO);
    ballBodyDef.userData = this;
    b2Body* body = world->CreateBody(&ballBodyDef);
    m_body = body;
    
    b2CircleShape circle;
    circle.m_radius = m_radius/PTM_RATIO;
    b2FixtureDef ballShapeDef;
    ballShapeDef.shape = &circle;
    ballShapeDef.density = 1.0f;
    ballShapeDef.friction = 0.0f;
    ballShapeDef.restitution = 1.0f;
    body->CreateFixture(&ballShapeDef);
    
    m_type = 6;
}

void RedCircle::draw(){
    
    float radi = this->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO;
    setGLColor(1, 0, 0, 1);
    drawFilledCircle(ccp(m_xpos, m_ypos), radi, 50);
    //ccDrawCircle(ccp(this->getPosition().x,this->getPosition().y), radi, 0, 100, false, 1, 1);
}

/////////////////////////////////////////////////////////////////////////////////

MovableBlock::MovableBlock(){
    
}

MovableBlock::~MovableBlock(){
    
    
}

bool MovableBlock::init(float radius, float xpos, float ypos){
    if(!CCSprite::init()){
        return false;
    }
    m_radius = radius;
    m_xpos = xpos;
    m_ypos = ypos;
    return true;
}

MovableBlock* MovableBlock::create(const float radius, const float xpos, const float ypos){
    MovableBlock *pobSprite = new MovableBlock();
    if (pobSprite && pobSprite->init(radius, xpos, ypos))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}

void MovableBlock::createBody(b2World *world){
    b2BodyDef ballBodyDef;
    ballBodyDef.type = b2_dynamicBody;
    ballBodyDef.position.Set(m_xpos/PTM_RATIO, m_ypos/PTM_RATIO);
    ballBodyDef.userData = this;
    b2Body* body = world->CreateBody(&ballBodyDef);
    m_body = body;
    
    b2CircleShape circle;
    circle.m_radius = m_radius/PTM_RATIO;
    b2FixtureDef ballShapeDef;
    ballShapeDef.shape = &circle;
    ballShapeDef.density = 1.0f;
    ballShapeDef.friction = 0.0f;
    ballShapeDef.restitution = 1.0f;
    body->CreateFixture(&ballShapeDef);
    
    m_type = 7;
}

void MovableBlock::draw(){
    
    float radi = this->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO;
    setGLColor(1, 1, 1, 1);
    drawFilledCircle(ccp(m_xpos, m_ypos), radi, 50);
    setGLColor(1, 0, 0, 1);
    ccDrawSolidRect(ccp(m_xpos - DEFAULT_RADIUS/2, m_ypos - DEFAULT_RADIUS/2), ccp(m_xpos + DEFAULT_RADIUS/2, m_ypos + DEFAULT_RADIUS / 2), ccc4f(1,0, 0, 1));
    //ccDrawCircle(ccp(this->getPosition().x,this->getPosition().y), radi, 0, 100, false, 1, 1);
}


////////////////////////////////////////////////////////////////////////////////////////
IceCircle::IceCircle(){
    
}

IceCircle::~IceCircle(){
    
    
}

bool IceCircle::init(float radius, float xpos, float ypos){
    if(!CCSprite::init()){
        return false;
    }
    m_radius = radius;
    m_xpos = xpos;
    m_ypos = ypos;
    return true;
}

IceCircle* IceCircle::create(const float radius, const float xpos, const float ypos){
    IceCircle *pobSprite = new IceCircle();
    if (pobSprite && pobSprite->init(radius, xpos, ypos))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;
}

void IceCircle::createBody(b2World *world){
    b2BodyDef ballBodyDef;
    ballBodyDef.type = b2_dynamicBody;
    ballBodyDef.position.Set(m_xpos/PTM_RATIO, m_ypos/PTM_RATIO);
    ballBodyDef.userData = this;
    b2Body* body = world->CreateBody(&ballBodyDef);
    m_body = body;
    
    b2CircleShape circle;
    circle.m_radius = m_radius/PTM_RATIO;
    b2FixtureDef ballShapeDef;
    ballShapeDef.shape = &circle;
    ballShapeDef.density = 1.0f;
    ballShapeDef.friction = 0.0f;
    ballShapeDef.restitution = 1.0f;
    body->CreateFixture(&ballShapeDef);
    
    m_type = 8;
}

void IceCircle::draw(){
    
    float radi = this->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO;
    setGLColor(0.4, 0.4, 1, 1);
    drawFilledCircle(ccp(m_xpos, m_ypos), radi, 50);
    //ccDrawCircle(ccp(this->getPosition().x,this->getPosition().y), radi, 0, 100, false, 1, 1);
}


