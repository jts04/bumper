//
//  SaveableDictionary.h
//  Viking
//
//  Created by soon on 13. 4. 11..
//
//

#ifndef __Viking__SaveableDictionary__
#define __Viking__SaveableDictionary__

#include <iostream>
#include "cocos2d.h"
USING_NS_CC;

class SaveableDictionary : public cocos2d::CCDictionary{

    SaveableDictionary();
    virtual ~SaveableDictionary();
    void WriteDictionary(CCDictionary* dic, FILE* fp, const char* key);
public:
    static SaveableDictionary* createWithContentsOfFile(const char* pFileName);
    static SaveableDictionary* create();
    void saveToFile(const char* path);
};

#endif /* defined(__Viking__SaveableDictionary__) */
