//
//  HelloWorldScene.cpp
//  Hundreds
//
//  Created by soon on 13. 6. 2..
//  Copyright __MyCompanyName__ 2013년. All rights reserved.
//
#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "MapMenu.h"

using namespace cocos2d;
using namespace CocosDenshion;

static MapMenu* m_mapMenu = NULL;

//#define PTM_RATIO 32

HelloWorld::HelloWorld():
m_isTouched(false),
m_playing(true),
m_menuOn(false),
#ifdef TOGGLE_CONTROL_MODE
m_selectedObjs(),
#else
m_selectedObj(NULL),
#endif
m_selectedBlock(NULL),
m_stage(STAGE_ID),
m_maxStage(MAX_STAGE),
m_scoreLabel(NULL),
m_dic(NULL)
{
#ifdef TOGGLE_CONTROL_MODE
    m_selectedObjs.clear();
#endif
}

HelloWorld::~HelloWorld()
{

}

bool HelloWorld::init(){
    if(!CCLayerColor::initWithColor(ccc4(50, 50, 50, 255))){
        return false;
    }
    
#ifdef MAP_TOOL_MODE
    this->setScale(0.5);
    this->setAnchorPoint(ccp(0,0));
#else
    CCSprite* background = CCSprite::create("background.jpg");
    background->setPosition(ccp(240,160));
    this->addChild(background, 0);
    background->setScale(0.5);
#endif
    
    initPhysics();
    setTouchEnabled( true );
    loadStage();
    
#ifdef MAP_TOOL_MODE
    m_mapMenu = MapMenu::create(m_world);
    m_mapMenu->setAnchorPoint(ccp(0,0));
    m_mapMenu->setPosition(ccp(0,0));
    m_mapMenu->setScale(2);
    m_mapMenu->setParentLayer(this);
    this->addChild(m_mapMenu, -1);
    m_mapMenu->updateInfo();
#endif
    
    float totalscore = 0;
    for(std::vector<HundredsObjects*>::iterator itr = m_objects.begin(); itr != m_objects.end(); itr++){
        HundredsObjects* obj = *itr;
        TouchableCircle* touchable = dynamic_cast<TouchableCircle*>(obj);
        if(touchable)
            totalscore += touchable->getPoint();
    }
    char* buf = new char[20];
    sprintf(buf, "%0.0f", totalscore);
    m_scoreLabel = CCLabelTTF::create(buf, "Thonburi", 40);
    delete [] buf;
    m_scoreLabel->setColor(ccc3(100, 100, 100));
    m_scoreLabel->setPosition(ccp(240,160));
    //this->addChild(m_scoreLabel);
    m_scoreLabel->retain();
    this->addChild(m_scoreLabel);
    scheduleUpdate();

    return true;
}

void HelloWorld::ccTouchesBegan(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent){

    CCSetIterator it;
    CCTouch* touch;
    
    for( it = pTouches->begin(); it != pTouches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        
        if(m_playing == false && m_menuOn == false){
            restart();
        }
        CCPoint location = touch->getLocationInView();
        
        location = CCDirector::sharedDirector()->convertToGL(location);
        float scale = this->getScale();
        
        for(std::vector<HundredsObjects*>::iterator itr = m_objects.begin(); itr!= m_objects.end(); itr++){
            HundredsObjects* obj = *itr;
            
            
#ifdef MAP_TOOL_MODE
            float distance = sqrtf((location.x - obj->getXpos() * scale) * (location.x - obj->getXpos() * scale) + (location.y - obj->getYpos() * scale) * (location.y - obj->getYpos() * scale));
            float radi = obj->getRadius() * scale;
            if(distance < radi){
                m_mapMenu->setNowSelectedObject(obj);
            }
            m_mapMenu->updateInfo();
            m_nowTouchedPoint = ccp(location.x / scale, location.y / scale);
#endif
            
            
            TouchableCircle* touchable = dynamic_cast<TouchableCircle*>(obj);
            if(touchable){
                float distance = sqrtf((location.x - obj->getXpos() * scale) * (location.x - obj->getXpos() * scale) + (location.y - obj->getYpos() * scale) * (location.y - obj->getYpos() * scale));
                float radi = touchable->getRadius() * scale;
                if(distance < radi){
                    m_isTouched = true;
                    
#ifdef TOGGLE_CONTROL_MODE
                    if(touchable->getToggleOn() == false){
#ifdef MAP_TOOL_MODE
                        if(m_mapMenu->getEditMode() == false && m_mapMenu->getRunOn() == true)
                            touchable->setToggleOn(true);
#else
                        touchable->setToggleOn(true);
#endif
                        m_selectedObjs.push_back(touchable);
                    }
                    else{
                        for(std::vector<TouchableCircle*>::iterator itr2 = m_selectedObjs.begin(); itr2 != m_selectedObjs.end(); itr2++){
                            
                            if(touchable == *itr2){
                                touchable->setToggleOn(false);
                                m_selectedObjs.erase(itr2);
                                OneTimeCircle* onetime = dynamic_cast<OneTimeCircle*>(touchable);
                                if(onetime)
                                    onetime->setTouched(true);
                                break;
                            }
                        }
                    }
#else
                    m_selectedObj = touchable;
#endif
                }
            }
            
            MovableBlock* movable = dynamic_cast<MovableBlock*>(obj);
            if(movable){
                float distance = sqrtf((location.x - movable->getXpos() * scale) * (location.x - movable->getXpos() * scale) + (location.y - movable->getYpos() * scale) * (location.y - movable->getYpos() * scale));
                float radi = movable->getRadius() * scale;
                if(distance < radi){
                    m_isTouched = true;
                    m_selectedBlock = movable;
                    
                    m_nowTouchedPoint = ccp(location.x / scale, location.y / scale);
#ifdef MAP_TOOL_MODE
                    //m_mapMenu->setNowSelectedObject(m_selectedBlock);
#endif
                }
            }
        }
        
    }
}
void HelloWorld::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
    //Add a new body/atlas sprite at the touched location
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        
        CCPoint location = touch->getLocationInView();
        
        location = CCDirector::sharedDirector()->convertToGL(location);
        
#ifdef TOGGLE_CONTROL_MODE
        
#else
        OneTimeCircle* onetime = dynamic_cast<OneTimeCircle*>(m_selectedObj);
        if(onetime)
            onetime->setTouched(true);
        
        m_isTouched = false;
        m_selectedObj = NULL;
#endif
        if(m_selectedBlock)
            m_selectedBlock->getBody()->SetType(b2_staticBody);
        m_selectedBlock = NULL;
        
    }
}

void HelloWorld::ccTouchesMoved(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent){
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = pTouches->begin(); it != pTouches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        
        CCPoint location = touch->getLocationInView();
        
        location = CCDirector::sharedDirector()->convertToGL(location);
        
#ifdef MAP_TOOL_MODE
        if(m_isTouched && (m_selectedBlock || m_mapMenu->getEditMode()))
            m_nowTouchedPoint = ccp(location.x / this->getScale(), location.y / this->getScale());
#else
        if(m_isTouched && m_selectedBlock)
            m_nowTouchedPoint = ccp(location.x / this->getScale(), location.y / this->getScale());
#endif
        
    }
}
void HelloWorld::draw(){
    CCLayerColor::draw();
    //m_scoreLabel->visit();
}

void HelloWorld::update(float dt)
{
    if(m_playing == false)
        return;
#ifdef MAP_TOOL_MODE
    
    if(m_mapMenu == NULL){
    
    }
    else{
        if(m_mapMenu->getRunOn() == false && m_mapMenu->getEditMode() == false)
            return;
        else if(m_mapMenu->getEditMode() == true){
            int velocityIterations = 8;
            int positionIterations = 1;
            
            // Instruct the world to perform a single step of simulation. It is
            // generally best to keep the time step and iterations fixed.
            m_world->Step(dt, velocityIterations, positionIterations);
            
            //Iterate over the bodies in the physics world
            for (b2Body* b = m_world->GetBodyList(); b; b = b->GetNext())
            {
                if (b->GetUserData() != NULL) {
                    //Synchronize the AtlasSprites position and rotation with the corresponding body
                    HundredsObjects* obj = (HundredsObjects*)b->GetUserData();
                    /*
                     TouchableCircle* touchable = dynamic_cast<TouchableCircle*>(obj);
                     if(touchable && touchable->getFreezePoint() != 0){
                     
                     }
                     else{
                     */
                    obj->setXpos(b->GetPosition().x * PTM_RATIO);
                    obj->setYpos(b->GetPosition().y * PTM_RATIO);
                    //}
                    //CCSprite* myActor = (CCSprite*)b->GetUserData();
                    //myActor->setPosition( CCPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO) );
                    //myActor->setRotation( -1 * CC_RADIANS_TO_DEGREES(b->GetAngle()) );
                }
            }
            raiseCircle();
            return;
        }
    }

#endif
    
    int velocityIterations = 8;
    int positionIterations = 1;
    
    // Instruct the world to perform a single step of simulation. It is
    // generally best to keep the time step and iterations fixed.
    m_world->Step(dt, velocityIterations, positionIterations);
    
    //Iterate over the bodies in the physics world
    for (b2Body* b = m_world->GetBodyList(); b; b = b->GetNext())
    {
        if (b->GetUserData() != NULL) {
            //Synchronize the AtlasSprites position and rotation with the corresponding body
            HundredsObjects* obj = (HundredsObjects*)b->GetUserData();
            /*
            TouchableCircle* touchable = dynamic_cast<TouchableCircle*>(obj);
            if(touchable && touchable->getFreezePoint() != 0){
            
            }
            else{
             */
                obj->setXpos(b->GetPosition().x * PTM_RATIO);
                obj->setYpos(b->GetPosition().y * PTM_RATIO);
            //}
            //CCSprite* myActor = (CCSprite*)b->GetUserData();
            //myActor->setPosition( CCPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO) );
            //myActor->setRotation( -1 * CC_RADIANS_TO_DEGREES(b->GetAngle()) );
        }
    }

    //충돌검사
#ifdef TOGGLE_CONTROL_MODE
    if(m_selectedObjs.size() != 0){
        for(std::vector<HundredsObjects*>::iterator itr = m_objects.begin(); itr != m_objects.end(); itr++){
            HundredsObjects* obj = *itr;
            
            for(std::vector<TouchableCircle*>::iterator itr2 = m_selectedObjs.begin(); itr2 != m_selectedObjs.end(); itr2++){
            
                TouchableCircle* m_selectedObj = *itr2;
                if(m_selectedObj->getToggleOn() == false)
                    continue;
                if(obj == m_selectedObj)
                    continue;
                float distance = sqrt((m_selectedObj->getXpos() - obj->getXpos()) * (m_selectedObj->getXpos() - obj->getXpos()) + (m_selectedObj->getYpos() - obj->getYpos()) * (m_selectedObj->getYpos() - obj->getYpos()));
                float radi = obj->getRadius();
                float radiSel = m_selectedObj->getRadius();
                
                if(distance -1<= radi + radiSel){
                    //game over!!
                    TouchableCircle* touchable = dynamic_cast<TouchableCircle*>(obj);
                    if(touchable && touchable->getFreezePoint() != 0){
                        touchable->warmed();
                        
                        //m_isTouched = false;
                        m_selectedObj->setToggleOn(false);
                        m_selectedObj = NULL;
                        break;
                    }
                    else{
                        gameOver();
                        return;
                    }
                }
            }
            
        }
    }
    
#else
    if(m_isTouched && m_selectedObj)
    for(std::vector<HundredsObjects*>::iterator itr = m_objects.begin(); itr != m_objects.end(); itr++){
        HundredsObjects* obj = *itr;
        
        if(obj == m_selectedObj)
            continue;
        float distance = sqrt((m_selectedObj->getXpos() - obj->getXpos()) * (m_selectedObj->getXpos() - obj->getXpos()) + (m_selectedObj->getYpos() - obj->getYpos()) * (m_selectedObj->getYpos() - obj->getYpos()));
        float radi = obj->getRadius();
        float radiSel = m_selectedObj->getRadius();
        
        if(distance <= radi + radiSel){
            //game over!!
            TouchableCircle* touchable = dynamic_cast<TouchableCircle*>(obj);
            if(touchable && touchable->getFreezePoint() != 0){
                touchable->warmed();
                m_isTouched = false;
                m_selectedObj = NULL;
                break;
            }
            else{
                gameOver();
                return;
            }
        }
        
    }
#endif
    
#ifdef TOGGLE_CONTROL_MODE
    //공 키우기
    if(m_isTouched && (!m_selectedObjs.empty()|| m_selectedBlock)){
        raiseCircle();
    }
#else
    //공 키우기
    if(m_isTouched && (m_selectedObj  || m_selectedBlock)){
        raiseCircle();
    }
#endif
    
    //sawcircle, redcircle, icecircle이 있는 경우 충돌 검사
    for(std::vector<HundredsObjects*>::iterator itr = m_objects.begin(); itr != m_objects.end(); itr++){
        HundredsObjects* obj = *itr;
        
        SawCircle* saw = dynamic_cast<SawCircle*>(obj);
        if(saw){
            for(std::vector<HundredsObjects*>::iterator itr2 = m_objects.begin(); itr2 != m_objects.end(); itr2++){
                
                TouchableCircle* touchable = dynamic_cast<TouchableCircle*>(*itr2);
                if(touchable){
                    float distance = sqrt((touchable->getXpos() - saw->getXpos()) * (touchable->getXpos() - saw->getXpos()) + (touchable->getYpos() - saw->getYpos()) * (touchable->getYpos() - saw->getYpos()));
                    float radi = saw->getRadius();
                    float radiSel = touchable->getRadius();
                    
                    if(distance <= radi + radiSel){
                        
#ifdef TOGGLE_CONTROL_MODE
                        if(touchable->getToggleOn() == true)
                            gameOver();
                        else
                            setCirclePoint(touchable, 0);
#else
                        if(m_selectedObj == touchable)
                            gameOver();
                        else
                            setCirclePoint(touchable, 0);
#endif
                        //normal->setRadius(10);
                    }
                }
            }
        }
        
        RedCircle* red = dynamic_cast<RedCircle*>(obj);
        if(red){
            for(std::vector<HundredsObjects*>::iterator itr2 = m_objects.begin(); itr2 != m_objects.end(); itr2++){
                
                HundredsObjects* allobject = *itr2;
                //빨간색 끼리 부딪혀도 죽어야 하나?
                RedCircle* anotherRed = dynamic_cast<RedCircle*>(allobject);
                if(anotherRed){
                    //continue;
                }
                if(red == allobject)
                    continue;
                float distance = sqrt((allobject->getXpos() - red->getXpos()) * (allobject->getXpos() - red->getXpos()) + (allobject->getYpos() - red->getYpos()) * (allobject->getYpos() - red->getYpos()));
                float radi = red->getRadius();
                float radiSel = allobject->getRadius();
                if(distance <= radi + radiSel){
                    gameOver();
                }
                
            }
        }
        
        IceCircle* ice = dynamic_cast<IceCircle*>(obj);
        if(ice){
            for(std::vector<HundredsObjects*>::iterator itr2 = m_objects.begin(); itr2 != m_objects.end(); itr2++){
                
                HundredsObjects* allobject = *itr2;
                
                TouchableCircle* touchable = dynamic_cast<TouchableCircle*>(allobject);
                if(touchable && touchable->getFreezePoint() == 0){
                    float distance = sqrt((touchable->getXpos() - ice->getXpos()) * (touchable->getXpos() - ice->getXpos()) + (touchable->getYpos() - ice->getYpos()) * (touchable->getYpos() - ice->getYpos()));
                    float radi = ice->getRadius();
                    float radiSel = allobject->getRadius();
                    if(distance <= radi + radiSel){
                        touchable->setForceBeforeFreeze(touchable->getBody()->GetLinearVelocity());
                        touchable->getBody()->SetLinearVelocity(b2Vec2(0, 0));
                        touchable->freeze();
                    }
                }
                
            }
        }
        
    }
    
    //infected가 있는 경우 reducing
    for(std::vector<HundredsObjects*>::iterator itr = m_objects.begin(); itr != m_objects.end(); itr++){
        HundredsObjects* obj = *itr;
        
        InfectedCircle* infected = dynamic_cast<InfectedCircle*>(obj);
        if(infected)
            infected->reduce();
        
    }
    
    
    
    float totalscore = 0;
    int touchableNum = 0;
    for(std::vector<HundredsObjects*>::iterator itr = m_objects.begin(); itr != m_objects.end(); itr++){
        HundredsObjects* obj = *itr;
        TouchableCircle* touchable = dynamic_cast<TouchableCircle*>(obj);
        if(touchable){
            totalscore += touchable->getPoint();
            touchableNum++;
            if(touchable->getPhoenixCount() >0)
                touchable->setPhoenixCount(touchable->getPhoenixCount() - 1);
        }
        OneTimeCircle* onetime = dynamic_cast<OneTimeCircle*>(obj);
        if(onetime && onetime->getTouched() == true)
            touchableNum--;
    }
    
    char* buf = new char[20];
    sprintf(buf, "%0.0f", totalscore);
    m_scoreLabel->setString(buf);
    delete [] buf;
    
    if(totalscore >= VICTORY)
        gameOver(true);
    else if(touchableNum == 0)
        gameOver();
    
}


CCScene* HelloWorld::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // add layer as a child to scene
    CCLayer* layer = HelloWorld::create();
    scene->addChild(layer);
    layer->release();
    
    return scene;
}

void HelloWorld::initPhysics()
{

    CCSize s = CCDirector::sharedDirector()->getWinSize();

    b2Vec2 gravity;
    gravity.Set(0.0f, 0.0f);
    m_world = new b2World(gravity);

    // Do we want to let bodies sleep?
    m_world->SetAllowSleeping(true);

    m_world->SetContinuousPhysics(true);

//     m_debugDraw = new GLESDebugDraw( PTM_RATIO );
//     world->SetDebugDraw(m_debugDraw);

    uint32 flags = 0;
    flags += b2Draw::e_shapeBit;
    //        flags += b2Draw::e_jointBit;
    //        flags += b2Draw::e_aabbBit;
    //        flags += b2Draw::e_pairBit;
    //        flags += b2Draw::e_centerOfMassBit;
    //m_debugDraw->SetFlags(flags);


    // Define the ground body.
    b2BodyDef groundBodyDef;
    groundBodyDef.position.Set(0, 0); // bottom-left corner

    // Call the body factory which allocates memory for the ground body
    // from a pool and creates the ground box shape (also from a pool).
    // The body is also added to the world.
    b2Body* groundBody = m_world->CreateBody(&groundBodyDef);

    // Define the ground box shape.
    b2EdgeShape groundBox;
    groundBody->SetType(b2_staticBody);

    // bottom
    groundBox.Set(b2Vec2(0,0), b2Vec2(s.width/PTM_RATIO,0));
    groundBody->CreateFixture(&groundBox,0);

    // top
    groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO));
    groundBody->CreateFixture(&groundBox,0);

    // left
    groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(0,0));
    groundBody->CreateFixture(&groundBox,0);

    // right
    groundBox.Set(b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,0));
    groundBody->CreateFixture(&groundBox,0);
    
    groundBody->SetType(b2_staticBody);
    groundBody->GetFixtureList()->SetFriction(0);
    groundBody->GetFixtureList()->SetRestitution(1.0);
}

void HelloWorld::loadStage(){
    if(m_dic != NULL){
        m_dic->release();
        m_dic = NULL;
    }
    std::string path = CCFileUtils::sharedFileUtils()->getWriteablePath();
    path.append("stages.plist");
    FILE* fp = fopen(path.c_str(), "r");
    if(fp == NULL){
        m_dic = SaveableDictionary::createWithContentsOfFile("stages.plist");
    }
    else{
        m_dic = SaveableDictionary::createWithContentsOfFile(path.c_str());
        fclose(fp);
    }
    m_dic->retain();
    
    char* temp = new char[20];
    sprintf(temp, "%d", m_stage);
    
    m_maxStage = m_dic->allKeys()->count();
    
    CCDictionary* leveldic = (CCDictionary*)m_dic->objectForKey(temp);
    delete [] temp;
    CCDictionary* circles = (CCDictionary*)leveldic->objectForKey("circles");
    
    CCArray* allkey = circles->allKeys();
    if(allkey != NULL){
    for(int i = 0; i<allkey->count(); i++){
        CCString* keystr = (CCString*)(allkey->objectAtIndex(i));
        CCDictionary* circledic = (CCDictionary*)circles->objectForKey(keystr->getCString());
        CCString* typestr = (CCString*)circledic->valueForKey("type");
        CCString* posx = (CCString*)circledic->valueForKey("positionX");
        CCString* posy = (CCString*)circledic->valueForKey("positionY");
        CCString* velx = (CCString*)circledic->valueForKey("velX");
        CCString* vely = (CCString*)circledic->valueForKey("velY");
        
        CCSize winSize = CCDirector::sharedDirector()->getWinSize();
        float realxpos = winSize.width * atof(posx->getCString());
        float realypos = winSize.height - winSize.height * atof(posy->getCString());
        if(strcmp(typestr->getCString(), "normal") == 0){
            
            NormalCircle* circle = NormalCircle::create(DEFAULT_RADIUS,0,realxpos, realypos);
            circle->createBody(m_world);
            circle->getBody()->ApplyLinearImpulse(b2Vec2(atof(velx->getCString()), atof(vely->getCString())), b2Vec2(circle->getXpos() / PTM_RATIO, circle->getYpos() / PTM_RATIO));
            this->addChild(circle);
            circle->setPercentage();
            m_objects.push_back(circle);
        }
        if(strcmp(typestr->getCString(), "onetime") == 0){
            
            OneTimeCircle* circle = OneTimeCircle::create(DEFAULT_RADIUS,0,realxpos, realypos);
            circle->createBody(m_world);
            circle->getBody()->ApplyLinearImpulse(b2Vec2(atof(velx->getCString()), atof(vely->getCString())), b2Vec2(circle->getXpos() / PTM_RATIO, circle->getYpos() / PTM_RATIO));
            this->addChild(circle);
            circle->setPercentage();
            m_objects.push_back(circle);
        }
        if(strcmp(typestr->getCString(), "fixed") == 0){
            FixedCircle* circle = FixedCircle::create(30, realxpos, realypos);
            circle->createBody(m_world);
            this->addChild(circle);
            m_objects.push_back(circle);
        }
        if(strcmp(typestr->getCString(), "minus") == 0){
            CCString* point = (CCString*)circledic->valueForKey("point");
            int pointi = atoi(point->getCString());
            float radi = pointi;
            if(pointi < 0)
                radi = -pointi +DEFAULT_RADIUS;
            MinusCircle* circle = MinusCircle::create(radi, pointi,realxpos, realypos);
            circle->createBody(m_world);
            circle->getBody()->ApplyLinearImpulse(b2Vec2(atof(velx->getCString()), atof(vely->getCString())), b2Vec2(circle->getXpos() / PTM_RATIO, circle->getYpos() / PTM_RATIO));
            circle->setPercentage();
            this->addChild(circle);
            m_objects.push_back(circle);
        }
        if(strcmp(typestr->getCString(), "saw") == 0){
            SawCircle* circle = SawCircle::create(DEFAULT_RADIUS, realxpos, realypos);
            circle->createBody(m_world);
            circle->getBody()->ApplyLinearImpulse(b2Vec2(atof(velx->getCString()), atof(vely->getCString())), b2Vec2(circle->getXpos() / PTM_RATIO, circle->getYpos() / PTM_RATIO));
            circle->getBody()->ApplyAngularImpulse(20);
            this->addChild(circle);
            m_objects.push_back(circle);
        }
        if(strcmp(typestr->getCString(), "red") == 0){
            RedCircle* circle = RedCircle::create(DEFAULT_RADIUS, realxpos, realypos);
            circle->createBody(m_world);
            circle->getBody()->ApplyLinearImpulse(b2Vec2(atof(velx->getCString()), atof(vely->getCString())), b2Vec2(circle->getXpos() / PTM_RATIO, circle->getYpos() / PTM_RATIO));
            this->addChild(circle);
            m_objects.push_back(circle);
        }
        if(strcmp(typestr->getCString(), "ice") == 0){
            IceCircle* circle = IceCircle::create(DEFAULT_RADIUS, realxpos, realypos);
            circle->createBody(m_world);
            circle->getBody()->ApplyLinearImpulse(b2Vec2(atof(velx->getCString()), atof(vely->getCString())), b2Vec2(circle->getXpos() / PTM_RATIO, circle->getYpos() / PTM_RATIO));
            this->addChild(circle);
            m_objects.push_back(circle);
        }
        if(strcmp(typestr->getCString(), "movable") == 0){
            MovableBlock* circle = MovableBlock::create(DEFAULT_RADIUS, realxpos, realypos);
            circle->createBody(m_world);
            circle->getBody()->ApplyLinearImpulse(b2Vec2(atof(velx->getCString()), atof(vely->getCString())), b2Vec2(circle->getXpos() / PTM_RATIO, circle->getYpos() / PTM_RATIO));
            this->addChild(circle);
            m_objects.push_back(circle);
        }
        if(strcmp(typestr->getCString(), "infected") == 0){
            CCString* point = (CCString*)circledic->valueForKey("point");
            int pointi = atoi(point->getCString());
            
            InfectedCircle* circle = InfectedCircle::create(DEFAULT_RADIUS + pointi, pointi, realxpos, realypos);
            circle->createBody(m_world);
            circle->getBody()->ApplyLinearImpulse(b2Vec2(atof(velx->getCString()), atof(vely->getCString())), b2Vec2(circle->getXpos() / PTM_RATIO, circle->getYpos() / PTM_RATIO));
            this->addChild(circle);
            circle->setPercentage();
            m_objects.push_back(circle);
        }
        m_objects.back()->setInitialXpos(realxpos);
        m_objects.back()->setInitialYpos(realypos);
        m_objects.back()->setInitialXvel(atof(velx->getCString()));
        m_objects.back()->setInitialYvel(atof(vely->getCString()));
        m_objects.back()->setIdx(atoi(keystr->getCString()));
    }
    }
}

void HelloWorld::raiseCircle(){
    
#ifdef MAP_TOOL_MODE
    if(m_mapMenu->getRunOn() == false && m_mapMenu->getEditMode() == false)
        return;
    else if(m_mapMenu->getEditMode() == true){
        HundredsObjects* movable = dynamic_cast<HundredsObjects*>(m_mapMenu->getNowSelectedObject());
        if(movable){
            float nowx = movable->getBody()->GetPosition().x * PTM_RATIO;
            float nowy = movable->getBody()->GetPosition().y * PTM_RATIO;
            
            float nextx = m_nowTouchedPoint.x;
            float nexty = m_nowTouchedPoint.y;
            
            float velx = movable->getBody()->GetLinearVelocity().x + (nextx - nowx) /PTM_RATIO;
            float vely = movable->getBody()->GetLinearVelocity().y + (nexty - nowy) / PTM_RATIO;
            
            velx *= 0.88;
            vely *= 0.88;
            
            if(fabs(velx) < 0.1)
                velx = 0;
            if(fabs(vely) < 0.1)
                vely = 0;
            
            if(velx == 0 && vely == 0)
                movable->getBody()->SetType(b2_staticBody);
            else
                movable->getBody()->SetType(b2_dynamicBody);
            
            movable->getBody()->SetLinearVelocity(b2Vec2(velx, vely));
            movable->getBody()->SetAngularVelocity(0);
            movable->getBody()->ApplyLinearImpulse(b2Vec2((nextx - nowx)/PTM_RATIO, (nexty - nowy)/PTM_RATIO), b2Vec2(nowx / PTM_RATIO, nowy /PTM_RATIO));
            movable->setInitialXpos(movable->getBody()->GetPosition().x * PTM_RATIO);
            movable->setInitialYpos(movable->getBody()->GetPosition().y * PTM_RATIO);
            m_mapMenu->updateInfo();
            
        }
        return;
    }
#endif
    
    
#ifdef TOGGLE_CONTROL_MODE
    for(std::vector<TouchableCircle*>::iterator itr = m_selectedObjs.begin(); itr != m_selectedObjs.end(); itr++){
        TouchableCircle* m_selectedObj = *itr;
        if(m_selectedObj->getToggleOn() == false)
            continue;
        
        TouchableCircle* touchable = dynamic_cast<TouchableCircle*>(m_selectedObj);
        if(touchable && touchable->getFreezePoint() != 0){
            touchable->setFreezePoint(touchable->getFreezePoint() - 1);
            if(touchable->getFreezePoint() == 0)
                touchable->warmed();
            return;
        }
        
        NormalCircle* normal = dynamic_cast<NormalCircle*>(m_selectedObj);
        float radi = 0;
        if(normal){
            normal->setPoint(normal->getPoint() + DEFAULT_RASING);
            normal->setRadius(normal->getRadius() + DEFAULT_RASING);
            normal->getBody()->GetFixtureList()->GetShape()->m_radius = normal->getRadius() / PTM_RATIO;
            radi = normal->getRadius();
            
            char* temp = new char[20];
            sprintf(temp, "%0.0f",normal->getPoint());
            normal->getPercentLabel()->setString(temp);
            normal->getPercentLabel()->setFontSize(normal->getPercentLabel()->getFontSize() + DEFAULT_RASING);
            delete[] temp;
        }
        
        MinusCircle* minus = dynamic_cast<MinusCircle*>(m_selectedObj);
        if(minus){
            minus->setPoint(minus->getPoint() + DEFAULT_RASING);
            float minus_radi = minus->getRadius();
            if(minus->getPoint() < 0)
                minus_radi = -1 * minus->getPoint() + DEFAULT_RADIUS;
            else if(minus->getPoint() < DEFAULT_RADIUS)
                minus_radi = DEFAULT_RADIUS;
            else
                minus_radi = minus->getPoint();
            
            m_selectedObj->setRadius(minus_radi);
            m_selectedObj->getBody()->GetFixtureList()->GetShape()->m_radius = minus_radi / PTM_RATIO;
            radi = minus->getPoint();
            
            char* temp = new char[20];
            sprintf(temp, "%0.0f",minus->getPoint());
            minus->getPercentLabel()->setString(temp);
            if(radi < 0)
                minus->getPercentLabel()->setFontSize(-radi + 20);
            else
                minus->getPercentLabel()->setFontSize(radi + 20);
            delete[] temp;
        }
        
        InfectedCircle* infected = dynamic_cast<InfectedCircle*>(m_selectedObj);
        if(infected){
            infected->setPoint(infected->getPoint() + DEFAULT_RASING);
            infected->setRadius(infected->getRadius() + DEFAULT_RASING);
            infected->getBody()->GetFixtureList()->GetShape()->m_radius = infected->getRadius() / PTM_RATIO;
            radi = infected->getRadius();
            
            char* temp = new char[20];
            sprintf(temp, "%0.0f",infected->getPoint());
            infected->getPercentLabel()->setString(temp);
            infected->getPercentLabel()->setFontSize(infected->getPercentLabel()->getFontSize() + DEFAULT_RASING);
            delete[] temp;
        }
        
        OneTimeCircle* onetime = dynamic_cast<OneTimeCircle*>(m_selectedObj);
        if(onetime && onetime->getTouched() == false){
            
            onetime->setPoint(onetime->getPoint() + DEFAULT_RASING);
            onetime->setRadius(onetime->getRadius() + DEFAULT_RASING);
            onetime->getBody()->GetFixtureList()->GetShape()->m_radius = onetime->getRadius() / PTM_RATIO;
            
            char* temp = new char[20];
            sprintf(temp, "%0.0f",onetime->getPoint());
            onetime->getPercentLabel()->setString(temp);
            onetime->getPercentLabel()->setFontSize(onetime->getPercentLabel()->getFontSize() + DEFAULT_RASING);
            delete[] temp;
        }
        
    }
    
    MovableBlock* movable = dynamic_cast<MovableBlock*>(m_selectedBlock);
    if(movable){
        float nowx = movable->getBody()->GetPosition().x * PTM_RATIO;
        float nowy = movable->getBody()->GetPosition().y * PTM_RATIO;
        
        float nextx = m_nowTouchedPoint.x;
        float nexty = m_nowTouchedPoint.y;
        
        float velx = movable->getBody()->GetLinearVelocity().x + (nextx - nowx) /PTM_RATIO;
        float vely = movable->getBody()->GetLinearVelocity().y + (nexty - nowy) / PTM_RATIO;
        
        velx *= 0.88;
        vely *= 0.88;
        
        if(fabs(velx) < 0.1)
            velx = 0;
        if(fabs(vely) < 0.1)
            vely = 0;
        
        if(velx == 0 && vely == 0)
            movable->getBody()->SetType(b2_staticBody);
        else
            movable->getBody()->SetType(b2_dynamicBody);
        
        movable->getBody()->SetLinearVelocity(b2Vec2(velx, vely));
        movable->getBody()->SetAngularVelocity(0);
        movable->getBody()->ApplyLinearImpulse(b2Vec2((nextx - nowx)/PTM_RATIO, (nexty - nowy)/PTM_RATIO), b2Vec2(nowx / PTM_RATIO, nowy /PTM_RATIO));
        
    }
    
#else

    TouchableCircle* touchable = dynamic_cast<TouchableCircle*>(m_selectedObj);
    if(touchable && touchable->getFreezePoint() != 0){
        touchable->setFreezePoint(touchable->getFreezePoint() - 1);
        if(touchable->getFreezePoint() == 0)
            touchable->warmed();
        return;
    }
    
    NormalCircle* normal = dynamic_cast<NormalCircle*>(m_selectedObj);
    float radi = 0;
    if(normal){
        normal->setPoint(normal->getPoint() + 1);
        normal->setRadius(normal->getRadius() + 1);
        normal->getBody()->GetFixtureList()->GetShape()->m_radius = normal->getRadius() / PTM_RATIO;
        radi = normal->getRadius();
        
        char* temp = new char[20];
        sprintf(temp, "%d",normal->getPoint());
        normal->getPercentLabel()->setString(temp);
        normal->getPercentLabel()->setFontSize(normal->getPercentLabel()->getFontSize() + 1);
        delete[] temp;
    }
    
    MinusCircle* minus = dynamic_cast<MinusCircle*>(m_selectedObj);
    if(minus){
        minus->setPoint(minus->getPoint() + 1);
        float minus_radi = minus->getRadius();
        if(minus->getPoint() < 0)
            minus_radi = -1 * minus->getPoint() + DEFAULT_RADIUS;
        else if(minus->getPoint() < DEFAULT_RADIUS)
            minus_radi = DEFAULT_RADIUS;
        else
            minus_radi = minus->getPoint();
        
        m_selectedObj->setRadius(minus_radi);
        m_selectedObj->getBody()->GetFixtureList()->GetShape()->m_radius = minus_radi / PTM_RATIO;
        radi = minus->getPoint();
        
        char* temp = new char[20];
        sprintf(temp, "%d",minus->getPoint());
        minus->getPercentLabel()->setString(temp);
        if(radi < 0)
            minus->getPercentLabel()->setFontSize(-radi + 20);
        else
            minus->getPercentLabel()->setFontSize(radi + 20);
        delete[] temp;
    }
    
    InfectedCircle* infected = dynamic_cast<InfectedCircle*>(m_selectedObj);
    if(infected){
        infected->setPoint(infected->getPoint() + 1);
        infected->setRadius(infected->getRadius() + 1);
        infected->getBody()->GetFixtureList()->GetShape()->m_radius = infected->getRadius() / PTM_RATIO;
        radi = infected->getRadius();
        
        char* temp = new char[20];
        sprintf(temp, "%d",infected->getPoint());
        infected->getPercentLabel()->setString(temp);
        infected->getPercentLabel()->setFontSize(infected->getPercentLabel()->getFontSize() + 1);
        delete[] temp;
    }
    
    OneTimeCircle* onetime = dynamic_cast<OneTimeCircle*>(m_selectedObj);
    if(onetime && onetime->getTouched() == false){
        
        onetime->setPoint(onetime->getPoint() + 1);
        onetime->setRadius(onetime->getRadius() + 1);
        onetime->getBody()->GetFixtureList()->GetShape()->m_radius = onetime->getRadius() / PTM_RATIO;
        
        char* temp = new char[20];
        sprintf(temp, "%d",onetime->getPoint());
        onetime->getPercentLabel()->setString(temp);
        onetime->getPercentLabel()->setFontSize(onetime->getPercentLabel()->getFontSize() + 1);
        delete[] temp;
    }
    
    MovableBlock* movable = dynamic_cast<MovableBlock*>(m_selectedBlock);
    if(movable){
        float nowx = movable->getBody()->GetPosition().x * PTM_RATIO;
        float nowy = movable->getBody()->GetPosition().y * PTM_RATIO;
        
        float nextx = m_nowTouchedPoint.x;
        float nexty = m_nowTouchedPoint.y;
        
        float velx = movable->getBody()->GetLinearVelocity().x;
        float vely = movable->getBody()->GetLinearVelocity().y;
        
        velx *= 0.92;
        vely *= 0.92;
        
        if(fabs(velx) < 0.3)
            velx = 0;
        if(fabs(vely) < 0.3)
            vely = 0;
        
        movable->getBody()->SetLinearVelocity(b2Vec2(velx, vely));
        movable->getBody()->SetAngularVelocity(0);
        movable->getBody()->ApplyLinearImpulse(b2Vec2((nextx - nowx)/PTM_RATIO, (nexty - nowy)/PTM_RATIO), b2Vec2(nowx / PTM_RATIO, nowy /PTM_RATIO));
        
    }
    
#endif
    
    //for 벽에 닿았을 때 멈추는 현상
    for(std::vector<HundredsObjects*>::iterator itr = m_objects.begin(); itr != m_objects.end(); itr++){
        HundredsObjects* obj = *itr;
        //left
        if(obj->getXpos() < obj->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO){
            obj->getBody()->ApplyForce(b2Vec2(1,0), b2Vec2(obj->getBody()->GetPosition().x * PTM_RATIO, obj->getBody()->GetPosition().y * PTM_RATIO));
        }
        //top
        if(320 - obj->getYpos() < obj->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO){
            obj->getBody()->ApplyForce(b2Vec2(0,-1), b2Vec2(obj->getBody()->GetPosition().x * PTM_RATIO, obj->getBody()->GetPosition().y * PTM_RATIO));
        }
        //right
        if(480 - obj->getXpos() < obj->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO){
            obj->getBody()->ApplyForce(b2Vec2(-1,0), b2Vec2(obj->getBody()->GetPosition().x * PTM_RATIO, obj->getBody()->GetPosition().y * PTM_RATIO));
        }
        //bottom
        if(obj->getYpos() < obj->getBody()->GetFixtureList()->GetShape()->m_radius * PTM_RATIO){
            obj->getBody()->ApplyForce(b2Vec2(0,1), b2Vec2(obj->getBody()->GetPosition().x * PTM_RATIO, obj->getBody()->GetPosition().y * PTM_RATIO));
        }
    }
}

void HelloWorld::setCirclePoint(HundredsObjects *circle, int point){
    TouchableCircle* normal = dynamic_cast<TouchableCircle*>(circle);
    //float radi = 0;
    if(normal){
        normal->setPoint(0);
        normal->setRadius(DEFAULT_RADIUS);
        normal->getBody()->GetFixtureList()->GetShape()->m_radius = (float)DEFAULT_RADIUS / PTM_RATIO;
        //radi = normal->getRadius();
        char* temp = new char[20];
        sprintf(temp, "%d",0);
        normal->getPercentLabel()->setString(temp);
        normal->getPercentLabel()->setFontSize(20);
        delete[] temp;
    }
}

void HelloWorld::gameOver(bool isWin){
    m_playing = false;
    m_isTouched = false;
    
    //CCSprite* gameoverspr = CCSprite::create("HelloWorld.png");
    //gameoverspr->setScale(0.5);
    //gameoverspr->setPosition(ccp(240,160));
    //this->addChild(gameoverspr, 0, 1);
    
    if(isWin)
        this->setColor(ccc3(0, 0, 255));
    else
        this->setColor(ccc3(255, 0, 0));
}

void HelloWorld::restart(){
#ifdef MAP_TOOL_MODE
    m_mapMenu->setNowSelectedObject(NULL);
#endif
    m_menuOn = true;
    //m_selectedObj = NULL;
    for(std::vector<HundredsObjects*>::iterator itr = m_objects.begin(); itr != m_objects.end(); itr++){
        HundredsObjects* obj = *itr;
        m_world->DestroyBody(obj->getBody());
        this->removeChild(obj, true);
    }
    m_objects.clear();
    
    m_selectedObjs.clear();
    
    CCSprite* btn_minus = CCSprite::create("ButtonMinus.png");
    CCSprite* btn_minus_sel = CCSprite::create("ButtonMinusSel.png");
    CCSprite* btn_plus = CCSprite::create("ButtonPlus.png");
    CCSprite* btn_plus_sel = CCSprite::create("ButtonPlusSel.png");
    CCSprite* btn_star = CCSprite::create("ButtonStar.png");
    CCSprite* btn_star_sel = CCSprite::create("ButtonStarSel.png");

    CCMenuItem* prevStage = CCMenuItemSprite::create(btn_minus, btn_minus_sel, this, menu_selector(HelloWorld::onStageButtonTapped));
    prevStage->setTag(m_stage - 1);
    prevStage->setPosition(ccp(-80,0));
    CCMenuItem* retry = CCMenuItemSprite::create(btn_star, btn_star_sel, this, menu_selector(HelloWorld::onStageButtonTapped));
    retry->setTag(m_stage);
    retry->setPosition(ccp(0,0));
    CCMenuItem* nextStage = CCMenuItemSprite::create(btn_plus, btn_plus_sel, this, menu_selector(HelloWorld::onStageButtonTapped));
    nextStage->setTag(m_stage + 1);
    nextStage->setPosition(ccp(80,0));
    
    CCMenu* menu = CCMenu::create(retry, NULL);
    if(m_stage > 1)
        menu->addChild(prevStage);
    if(m_stage < m_maxStage)
        menu->addChild(nextStage);
    
    menu->setPosition(ccp(240,100));
    this->addChild(menu, 0, 2);
    
    //this->scheduleUpdate();
}

void HelloWorld::onStageButtonTapped(cocos2d::CCMenuItem *item){
    
    //this->removeChildByTag(1, true);
    int nextstage = item->getTag();
    //this->removeChildByTag(2, true);
    this->removeChild(item->getParent(), true);
    m_stage = nextstage;
    loadStage();
    m_playing = true;
    m_menuOn = false;
    this->setColor(ccc3(0, 0, 0));
}

