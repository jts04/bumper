//
//  HelloWorldScene.h
//  Hundreds
//
//  Created by soon on 13. 6. 2..
//  Copyright __MyCompanyName__ 2013년. All rights reserved.
//
#ifndef __HELLO_WORLD_H__
#define __HELLO_WORLD_H__

// When you import this file, you import all the cocos2d classes
#include "cocos2d.h"
#include "Box2D.h"
#include "NormalCircle.h"
#include "SaveableDictionary.h"

#define STAGE_ID 10
#define VICTORY 150

#define MAX_STAGE 11
#define DEFAULT_RASING 0.5

#define MAP_TOOL_MODE

class HelloWorld : public cocos2d::CCLayerColor {
private:
    
    ~HelloWorld();
    HelloWorld();
    virtual bool init();
    virtual void ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    virtual void draw();
    void update(float dt);
    
public:
    
    CREATE_FUNC(HelloWorld);
    static cocos2d::CCScene* scene();
    void initPhysics();
    CC_SYNTHESIZE(b2World*, m_world, World);
    CC_SYNTHESIZE(std::vector<HundredsObjects*>, m_objects, Objects);
    CC_SYNTHESIZE(bool, m_isTouched, IsTouched);
    CC_SYNTHESIZE(bool, m_playing, Playing);
    CC_SYNTHESIZE(bool, m_menuOn, MenuOn);
#ifdef TOGGLE_CONTROL_MODE
    CC_SYNTHESIZE(std::vector<TouchableCircle*>, m_selectedObjs, SelectedObjs);
#else
    CC_SYNTHESIZE(TouchableCircle*, m_selectedObj, SelectedObj);
#endif
    CC_SYNTHESIZE(MovableBlock*, m_selectedBlock, SelectedBlock);
    CC_SYNTHESIZE(int, m_stage, Stage);
    CC_SYNTHESIZE(int, m_maxStage, MaxStage);
    CC_SYNTHESIZE(CCLabelTTF*, m_scoreLabel, ScoreLabel);
    CC_SYNTHESIZE(CCPoint, m_nowTouchedPoint, NowTouchedPoint);
    CC_SYNTHESIZE(SaveableDictionary*, m_dic, Dic);
    void loadStage();
    void raiseCircle();
    void setCirclePoint(HundredsObjects* circle, int point);
    void gameOver(bool isWin = false);
    void restart();
    void onStageButtonTapped(CCMenuItem* item);
};

#endif // __HELLO_WORLD_H__
