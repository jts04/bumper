//
//  SaveableDictionary.cpp
//  Viking
//
//  Created by soon on 13. 4. 11..
//
//

#include "SaveableDictionary.h"

SaveableDictionary::SaveableDictionary(){

}

SaveableDictionary::~SaveableDictionary(){

}

SaveableDictionary* SaveableDictionary::createWithContentsOfFile(const char *pFileName){

    SaveableDictionary* newDictionary = (SaveableDictionary*)CCDictionary::createWithContentsOfFile(pFileName);
    
    //newDictionary->autorelease();
    return newDictionary;
}

SaveableDictionary* SaveableDictionary::create(){
    SaveableDictionary* newDictionary = (SaveableDictionary*)CCDictionary::create();
    
    //newDictionary->autorelease();
    return newDictionary;
}

void SaveableDictionary::WriteDictionary(CCDictionary* dic, FILE *fp, const char* key){
    
    char* buf = new char[200];
    sprintf(buf, "<key>%s</key>\n", key);
    fwrite(buf, strlen(buf), 1, fp);
    memset(buf, 0, strlen(buf));
    
    sprintf(buf, "<dict>\n");
    fwrite(buf, strlen(buf), 1, fp);
    memset(buf, 0, strlen(buf));
    
    if(dic->allKeys() != NULL){
        for(int i = 0; i<dic->allKeys()->count(); i++){
            CCString* nowkey = dynamic_cast<CCString*>(dic->allKeys()->objectAtIndex(i));
            if(nowkey){
                CCObject* nowobj = dic->objectForKey(nowkey->getCString());
                CCString* isString = dynamic_cast<CCString*>(nowobj);
                if(isString){
                    
                    std::string writeKey = nowkey->getCString();
                    std::string writeSome = isString->getCString();
                    char* tempkey = new char[strlen(writeKey.c_str()) + 20];
                    char* temp = new char[strlen(writeSome.c_str()) + 20];
                    sprintf(tempkey, "<key>%s</key>\n", writeKey.c_str());
                    sprintf(temp, "<string>%s</string>\n", writeSome.c_str());
                    
                    fwrite(tempkey, strlen(tempkey), 1, fp);
                    fwrite(temp, strlen(temp), 1, fp);
                    
                    delete [] temp;
                }
                CCDictionary* isDic = dynamic_cast<CCDictionary*>(nowobj);
                if(isDic){
                    /*recursive call!*/
                    WriteDictionary(isDic, fp, nowkey->getCString());
                }
            }
        }
    }
    
    sprintf(buf, "</dict>\n");
    fwrite(buf, strlen(buf), 1, fp);
    memset(buf, 0, strlen(buf));
    
    delete [] buf;
}

void SaveableDictionary::saveToFile(const char *path){
    FILE* fp = fopen(path, "w");
    if(fp != NULL){
        char* buf = new char[200];
        sprintf(buf, "%s", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        fwrite(buf, strlen(buf), 1, fp);
        memset(buf, 0, strlen(buf));
        
        sprintf(buf, "%s", "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n");
        fwrite(buf, strlen(buf), 1, fp);
        memset(buf, 0, strlen(buf));
        
        sprintf(buf, "%s", "<plist version=\"1.0\">\n");
        fwrite(buf, strlen(buf), 1, fp);
        memset(buf, 0, strlen(buf));
        
        sprintf(buf, "%s", "<dict>\n");
        fwrite(buf, strlen(buf), 1, fp);
        memset(buf, 0, strlen(buf));
        
        
        for(int i = 0; i<this->allKeys()->count(); i++){
            CCString* nowkey = dynamic_cast<CCString*>(this->allKeys()->objectAtIndex(i));
            if(nowkey){
                CCObject* nowobj = this->objectForKey(nowkey->getCString());
                CCString* isString = dynamic_cast<CCString*>(nowobj);
                if(isString){
                    
                    std::string writeKey = nowkey->getCString();
                    std::string writeSome = isString->getCString();
                    char* tempkey = new char[strlen(writeKey.c_str()) + 20];
                    char* temp = new char[strlen(writeSome.c_str()) + 20];
                    sprintf(tempkey, "<key>%s</key>\n", writeKey.c_str());
                    sprintf(temp, "<string>%s</string>\n", writeSome.c_str());

                    fwrite(tempkey, strlen(tempkey), 1, fp);
                    fwrite(temp, strlen(temp), 1, fp);
                    
                    delete [] temp;
                }
                CCDictionary* isDic = dynamic_cast<CCDictionary*>(nowobj);
                if(isDic){
                    WriteDictionary(isDic, fp, nowkey->getCString());
                }
            }
        }
        
        sprintf(buf, "%s", "</dict>\n");
        fwrite(buf, strlen(buf), 1, fp);
        memset(buf, 0, strlen(buf));
        
        sprintf(buf, "%s", "</plist>\n");
        fwrite(buf, strlen(buf), 1, fp);
        memset(buf, 0, strlen(buf));
        delete [] buf;
    }
    else{
        CCLog("error from opening file");
    }
    fclose(fp);
}
