//
//  NormalCircle.h
//  Hundreds
//
//  Created by soon on 13. 6. 2..
//
//

#ifndef __Hundreds__NormalCircle__
#define __Hundreds__NormalCircle__

#include <iostream>
#include "cocos2d.h"
#include "Box2D.h"
#define PTM_RATIO 32

#define TOGGLE_CONTROL_MODE
#define INFECTED_REDUCING_TIME 30
#define DEFAULT_RADIUS 20
#define DEFAULT_FREEZE_POINT 120
#define DEFAULT_PHOENIX_COUNT 10
USING_NS_CC;
class HundredsObjects : public CCSprite{
public:
    HundredsObjects();
    virtual ~HundredsObjects();
    
    CC_SYNTHESIZE(b2Body*, m_body, Body);
    CC_SYNTHESIZE(int, m_type, Type);
    CC_SYNTHESIZE(float, m_xpos, Xpos);
    CC_SYNTHESIZE(float, m_ypos, Ypos);
    CC_SYNTHESIZE(float, m_radius, Radius);
    
    /////////////////////////////////////////////for map tool
    
    CC_SYNTHESIZE(float, m_initialXpos, InitialXpos);
    CC_SYNTHESIZE(float, m_initialYpos, InitialYpos);
    CC_SYNTHESIZE(float, m_initialXvel, InitialXvel);
    CC_SYNTHESIZE(float, m_initialYvel, InitialYvel);
    
    CC_SYNTHESIZE(int, m_idx, Idx);
    

    void setGLColor(float r, float g, float b, float a);
    void drawFilledCircle(CCPoint center, float r, int totalSegs);
    
    
};
///////////////////////////////////////////////
class TouchableCircle : public HundredsObjects{
    
public:
    
    TouchableCircle();
    virtual ~TouchableCircle();
    
    void freeze();
    void warmed();
    CC_SYNTHESIZE(CCLabelTTF*, m_percentLabel, PercentLabel);
    CC_SYNTHESIZE(float, m_point, Point);
    CC_SYNTHESIZE(int, m_freezePoint, FreezePoint);
    CC_SYNTHESIZE(b2Vec2, m_forceBeforeFreeze, ForceBeforeFreeze);
    CC_SYNTHESIZE(int, m_phoenixCount, PhoenixCount);
#ifdef TOGGLE_CONTROL_MODE
    CC_SYNTHESIZE(bool, m_toggleOn, ToggleOn);
#endif
};
///////////////////////////////////////////////
class NormalCircle : public TouchableCircle{
public:
    NormalCircle();
    virtual ~NormalCircle();
    
    virtual bool init(float radius, int point, float xpos, float ypos);
    static NormalCircle* create(const float radius, const int point, const float xpos, const float ypos);
    void createBody(b2World* world);
    void setPercentage();
    virtual void draw();
    
};
////////////////////////////////////////////////
class FixedCircle : public HundredsObjects{
public:
    FixedCircle();
    virtual ~FixedCircle();
    
    virtual bool init(float radius, float xpos, float ypos);
    static FixedCircle* create(const float radius, const float xpos, const float ypos);
    void createBody(b2World* world);
    virtual void draw();
};
/////////////////////////////////////////////////
class MinusCircle : public TouchableCircle{
public:
    MinusCircle();
    virtual ~MinusCircle();
    
    virtual bool init(float radius, int point, float xpos, float ypos);
    static MinusCircle* create(const float radius, const int point, const float xpos, const float ypos);
    void createBody(b2World* world);
    void setPercentage();
    virtual void draw();
};

///////////////////////////////////////////////////
class InfectedCircle : public TouchableCircle{
public:
    InfectedCircle();
    virtual ~InfectedCircle();

    virtual bool init(float radius, int point, float xpos, float ypos);
    static InfectedCircle* create(const float radius, const int point, const float xpos, const float ypos);
    void createBody(b2World* world);
    void setPercentage();
    virtual void draw();
    CC_SYNTHESIZE(int, m_remainTimeToReduce, RemainTimeToReduce);
    void reduce();

};

///////////////////////////////////////////////////

class OneTimeCircle : public TouchableCircle{
public:
    OneTimeCircle();
    virtual ~OneTimeCircle();
    
    virtual bool init(float radius, int point, float xpos, float ypos);
    static OneTimeCircle* create(const float radius, const int point, const float xpos, const float ypos);
    void createBody(b2World* world);
    void setPercentage();
    virtual void draw();
    CC_SYNTHESIZE(bool, m_touched, Touched);
        
};
///////////////////////////////////////////////////
class SawCircle : public HundredsObjects{
public:
    SawCircle();
    virtual ~SawCircle();
    
    virtual bool init(float radius, float xpos, float ypos);
    static SawCircle* create(const float radius, const float xpos, const float ypos);
    void createBody(b2World* world);
    virtual void draw();
    
    CC_SYNTHESIZE(CCPoint*, m_sharp, Sharp);
};
////////////////////////////////////////////////////

class RedCircle : public HundredsObjects{
public:
    RedCircle();
    virtual ~RedCircle();
    
    virtual bool init(float radius, float xpos, float ypos);
    static RedCircle* create(const float radius, const float xpos, const float ypos);
    void createBody(b2World* world);
    virtual void draw();
};

////////////////////////////////////////////////////

class MovableBlock : public HundredsObjects{
public:
    MovableBlock();
    virtual ~MovableBlock();
    
    virtual bool init(float radius, float xpos, float ypos);
    static MovableBlock* create(const float radius, const float xpos, const float ypos);
    void createBody(b2World* world);
    virtual void draw();
};
////////////////////////////////////////////////////

class IceCircle : public HundredsObjects{
public:
    IceCircle();
    virtual ~IceCircle();
    
    virtual bool init(float radius, float xpos, float ypos);
    static IceCircle* create(const float radius, const float xpos, const float ypos);
    void createBody(b2World* world);
    virtual void draw();
};

#endif /* defined(__Hundreds__NormalCircle__) */
