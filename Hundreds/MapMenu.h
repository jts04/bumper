//
//  MapMenu.h
//  Hundreds
//
//  Created by soon on 13. 6. 5..
//
//

#ifndef __Hundreds__MapMenu__
#define __Hundreds__MapMenu__

#include <iostream>

#include "cocos2d.h"
#include "Box2D.h"
#include "HelloWorldScene.h"

USING_NS_CC;
class MapMenu : public CCLayerColor{
public:
    MapMenu();
    virtual ~MapMenu();
    virtual bool init(b2World* world);
    static MapMenu* create(b2World* world);
    
    CC_SYNTHESIZE(bool, m_runOn, RunOn);
    CC_SYNTHESIZE(bool, m_isEditMode, EditMode);
    CC_SYNTHESIZE(b2World*, m_world, World);
    CC_SYNTHESIZE(CCMenuItemSprite*, m_editMenuItem, EditMenuItem);
    CC_SYNTHESIZE(HundredsObjects*, m_nowSelectedObject, NowSelectedObject);
    CC_SYNTHESIZE(CCLabelTTF*, m_typeLabel, TypeLabel);
    CC_SYNTHESIZE(CCLabelTTF*, m_xposLabel, XposLabel);
    CC_SYNTHESIZE(CCLabelTTF*, m_yposLabel, YposLabel);
    CC_SYNTHESIZE(CCLabelTTF*, m_xvelLabel, XvelLabel);
    CC_SYNTHESIZE(CCLabelTTF*, m_yvelLabel, YvelLabel);
    CC_SYNTHESIZE(CCLabelTTF*, m_stageLabel, StageLabel);
    CC_SYNTHESIZE(HelloWorld*, m_parentLayer, ParentLayer);

    void onRunButtonTapped();
    void onEditButtonTapped();
    void onRemoveButtonTapped();
    void onSaveButtonTapped();
    void onNewLevelButtonTapped();
    void onChangeVelocityButtonTapped(CCMenuItem* item);
    void onGameOverButtonTapped();
    void onEditPositionTapped(CCMenuItem* item);
    void onEditConfirm(CCMenuItem* item);
    void onEditCancel();
    virtual void onEnter();
    virtual void onExit();
    virtual void ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    void updateInfo();
};

#endif /* defined(__Hundreds__MapMenu__) */
